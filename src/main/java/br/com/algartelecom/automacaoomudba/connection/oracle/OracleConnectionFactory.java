package br.com.algartelecom.automacaoomudba.connection.oracle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 
 * @author mourao
 *
 */
 public abstract class OracleConnectionFactory {

	 protected String hostname;
	 protected String username;
	 protected String password;
	
	/**
	 * Efetua uma conexao com o banco 
	 * @return	Uma conexao valida com o banco
	 * @throws SQLException 
	 */
	public Connection getConnection() throws SQLException {
	    
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		Connection conn = DriverManager.getConnection(hostname, username, password);

		return conn;
	}

}