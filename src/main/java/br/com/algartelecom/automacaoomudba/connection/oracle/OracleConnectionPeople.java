package br.com.algartelecom.automacaoomudba.connection.oracle;

import java.io.IOException;

import br.com.algartelecom.automacaoomudba.utils.OmuProperties;

/**
 * 
 * @author mourao
 *
 */
public class OracleConnectionPeople extends OracleConnectionFactory {

	public OracleConnectionPeople(String base) throws IOException, InterruptedException {

		hostname = OmuProperties.getProperty("people.oracle." + base + ".hostname");
		username = OmuProperties.getProperty("people.oracle." + base + ".username");
		password = OmuProperties.getPeopleOraclePassword(base);
	}
}
