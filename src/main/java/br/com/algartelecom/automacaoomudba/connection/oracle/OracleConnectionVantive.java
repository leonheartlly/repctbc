package br.com.algartelecom.automacaoomudba.connection.oracle;

import java.io.IOException;

import br.com.algartelecom.automacaoomudba.utils.OmuProperties;

/**
 * 
 * @author mourao
 *
 */
public class OracleConnectionVantive extends OracleConnectionFactory {
	
	public OracleConnectionVantive(String base) throws IOException, InterruptedException {

		hostname = OmuProperties.getProperty("vantive.oracle." + base + ".hostname");
		username = OmuProperties.getProperty("vantive.oracle." + base + ".username");
		password = OmuProperties.getVantiveOraclePassword(base);
	}

}
