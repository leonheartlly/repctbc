package br.com.algartelecom.automacaoomudba.main;

import java.io.File;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.algartelecom.automacaoomudba.omu.OmuException;
import br.com.algartelecom.automacaoomudba.omu.OmuManager;
import br.com.algartelecom.automacaoomudba.utils.OmuMail;
import br.com.algartelecom.automacaoomudba.utils.OmuProperties;
import br.com.algartelecom.automacaoomudba.utils.OmuUtil;
import br.com.algartelecom.automacaoomudba.utils.OmuZipUtil;

public class Main {

	private static Logger logger = LoggerFactory.getLogger(Main.class);
	
	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
    public static void main(String[] args) {
		
		String basename = args[0];
		String demand = args[1];
		String demandDir = null;
		
		OmuManager manager = new OmuManager();
		boolean existDemands = false;
		
		String originBase = OmuUtil.getOriginBase(demand);
		String destinationBase = OmuUtil.getDestinationBase(demand);
		
		String emailBodyText = null;
	    File attachment = null;

		try {
			
		    if (demand.equals("simulado")) {
		        demandDir = OmuProperties.getProperty("path.demands")+basename+"-simulado";
		        logger.debug(demandDir);
		    } else if (demand.equals("homologacao")) {
		        demandDir = OmuProperties.getProperty("path.demands")+basename+"-homologacao";
		        logger.debug(demandDir);
		    }

		    logger.info("-----------------------------------------------------------");
	        logger.info("--- Inicio do processo OMU " + basename.toUpperCase() + " " + demand.toUpperCase());
		    
		    existDemands = manager.extractDemands(demandDir, OmuProperties.getProperty("path.packages"));
		    if (existDemands) {
		        
		        File packageFile = manager.createOmuPackageToProcess(OmuProperties.getProperty("path.packages"), OmuProperties.getProperty("path.backup"), basename, demand);
		        attachment = OmuZipUtil.compress(packageFile, OmuProperties.getProperty("path.packages"));

	            // People
	            if (basename.equals("people")) {
	                
	                logger.info("Inicio da execucao do processo para os ambientes People");

	                Map<String, String> peopleScriptsResult = manager.runPeopleScripts(destinationBase);
	                Map<String, String> peopleDdlsResult = manager.runPeopleDdls(destinationBase);
	                String peopleCompileResult = manager.compilePeopleObjects(destinationBase);

	                // People Projects
	                Map<String, String> peopleProjectsResult = manager.updatePeopleProjects(originBase, destinationBase);
	                
	                emailBodyText = OmuUtil.buildBodyText(
	                        OmuUtil.getUpdateObjects(peopleScriptsResult, peopleDdlsResult, peopleProjectsResult), 
	                        OmuUtil.getNotUpdateObjects(peopleScriptsResult, peopleDdlsResult, peopleProjectsResult),
	                        peopleCompileResult);
	                
	                logger.info("Fim da execucao do processo para os ambientes People");
	                
	            }
	            // Vantive
	            else if (basename.equals("vantive")) {
	                
	                logger.info("Inicio da execucao do processo para os ambientes Vantive");
	                
	                Map<String, String> vantiveScriptsResult = manager.runVantiveScripts(destinationBase);
	                Map<String, String> vantiveDdlsResult = manager.runVantiveDdls(destinationBase);
	                String vantiveCompileResult = manager.compileVantiveObjects(destinationBase);
	              
	                // Vantive Omu
	                Map<String, String> vantiveOmuResult = executeOmuProcess(manager, originBase, destinationBase, new File(packageFile.getAbsoluteFile(), packageFile.getName()+".omu"));
	                
	                emailBodyText = "\n\n" + OmuUtil.buildBodyText(
                            OmuUtil.getUpdateObjects(vantiveScriptsResult, vantiveDdlsResult, vantiveOmuResult), 
                            OmuUtil.getNotUpdateObjects(vantiveScriptsResult, vantiveDdlsResult, vantiveOmuResult), 
                            vantiveCompileResult);
	                
	                logger.info(emailBodyText);
	                logger.info("Fim da execucao do processo para os ambientes Vantive");
	                
	            }
		    }

		    OmuMail.sendMail("OMU " + basename.toUpperCase() + " " + demand.toUpperCase(), emailBodyText, attachment);
            
            manager.deleteFiles(demandDir);
            manager.deleteFiles(OmuProperties.getProperty("path.packages"));
		    
		} catch (OmuException e) {
		    
			logger.debug("", e);
			logger.info("--- Processo OMU nao foi finalizado corretamente ----------");
			logger.info("-----------------------------------------------------------");
			
		}
		
		logger.info("--- Fim do processo OMU -----------------------------------");
		logger.info("-----------------------------------------------------------");

	}

	// private methods ----------------------------------------------------------------------------
	
	private static Map<String, String>  executeOmuProcess(OmuManager manager, String originBase, String destinationBase, File omuFile) throws OmuException {

	    int exportResult = -1;
	    String message = null;
	    Map<String, String> omuResult = null;
	    
	    // export
	    if (omuFile != null && omuFile.exists()) {
	        if (manager.sendFile(omuFile) == 0) {
	            exportResult = manager.executeOmu(
	                    OmuProperties.getProperty("vantive.oracle."+originBase+".host"), 
	                    OmuProperties.getProperty("vantive.oracle."+originBase+".env"), 
	                    "E", omuFile.getName()); 
	            if (exportResult == 0) {
	                message = "EXPORT do arquivo .omu executado com sucesso";
	            } else {
	                message = "EXPORT do arquivo .omu nao foi executado corretamente";
	            }
	        } else {
	            message = "Arquivo .omu nao foi enviado para o servidor";
            }
	    } else {
	        message = "Nao existe arquivo .omu na demanda";
        }
	    // import
	    if (exportResult == 0) {
	        int importResult = manager.executeOmu(
                    OmuProperties.getProperty("vantive.oracle."+destinationBase+".host"), 
                    OmuProperties.getProperty("vantive.oracle."+destinationBase+".env"), 
                    "I", omuFile.getName());
            if (importResult == 0) {
                omuResult = manager.getOmuVantiveResult("OK");
                message += ". IMPORT do arquivo .omu executado com sucesso";
            } else {
                omuResult = manager.getOmuVantiveResult("NO");
                message += ". IMPORT do arquivo .omu nao foi executado corretamente";
            }
	    }
	    
	    logger.info(message);
	    
	    return omuResult;
	}
	
}