package br.com.algartelecom.automacaoomudba.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.algartelecom.automacaoomudba.omu.packages.OmuPackage;

/**
 * Contem metodos utilitarios para a realizacao do processo OMU
 * @author mourao
 *
 */
public class OmuUtil {

	private static Logger logger = LoggerFactory.getLogger(OmuUtil.class);
	
	private OmuUtil() {
		
	}

	/**
	 * Obtem o descritor da base de origem dependendo do tipo da demanda
	 * @param demand	O tipo da demanda
	 * @return			O descritor da base de origem
	 */
	public static String getOriginBase(String demand) {
		
		StringBuilder base = new StringBuilder();
		
		if ("simulado".equals(demand)) {
			base.append("des");
		} else if ("homologacao".equals(demand)) {
			base.append("sim");
		}

		return base.toString();
	}

	/**
	 * Obtem o descritor da base de destino dependendo do tipo da demanda
	 * @param demand	O tipo da demanda
	 * @return			O descritor da base de destino
	 */
	public static String getDestinationBase(String demand) {

		StringBuilder base = new StringBuilder();
		
		if ("simulado".equals(demand)) {
			base.append("sim");
		} else if ("homologacao".equals(demand)) {
			base.append("hml");
		}

		return base.toString();
	}

	/**
	 * Cria um pacote de arquivos para o OMU
	 * @param packageFile	O diretorio de arquivos para o omu
	 * @return				O pacote OMU criado
	 * @throws IOException
	 */
	public static OmuPackage createOmuDemandPackage(File packageFile) throws IOException {
		
		File omuFile = null;
		File objectsFile = null;
		File ddlsPeople = null;
		File ddlsVantive = null;
		File scriptsPeople = null;
		File scriptsVantive = null;
		File backupFiles = null;
		
		for (String packageFileName : packageFile.list()) {

		    if ("omu_file.omu".equals(packageFileName)) {
				omuFile = new File(packageFile, packageFileName);
			} else if ("objects".equals(packageFileName)) {
				objectsFile = new File(packageFile, packageFileName);
			} else if ("DDLs_People".equals(packageFileName)) {
				ddlsPeople = new File(packageFile, packageFileName);
			} else if ("DDLs_Vantive".equals(packageFileName)) {
				ddlsVantive = new File(packageFile, packageFileName);
			} else if ("Scripts_People".equals(packageFileName)) {
				scriptsPeople = new File(packageFile, packageFileName);
			} else if ("Scripts_Vantive".equals(packageFileName)) {
				scriptsVantive = new File(packageFile, packageFileName);
			} else if ("BackupFiles".equals(packageFileName)) {
				backupFiles = new File(packageFile, packageFileName);
			}
		}
		
		return new OmuPackage(omuFile, objectsFile, ddlsPeople, ddlsVantive, scriptsPeople, scriptsVantive, backupFiles);
	}

	/**
	 * Obtem os objetos atualizados durante o processo
	 * @param objectsList  O conjunto dos objetos executados no processo
	 * @return             Uma lista com os objetos atualizados no processo
	 */
	public static String getUpdateObjects(Map<String, String>... objectsList) {
	    
	    StringBuilder builder = new StringBuilder();
	    
	    for (Map<String, String> objects : objectsList) {
	        if (objects != null) {
	            for (Map.Entry<String, String> entry : objects.entrySet()) {
	                logger.debug(entry.getKey() + ": " + entry.getValue());
	                if (entry.getValue().equals("OK")) {
	                    builder.append("\t" + entry.getKey() + "\n");
	                }
	            }
	        }
	    }
	    
	    return builder.toString();
	}
	
	/**
	 * Obtem os objetos nao atualizados durante o processo
	 * @param objectsList  O conjunto dos objetos executados no processo
	 * @return             Uma lista com os objetos nao atualizados no processo
	 */
	public static String getNotUpdateObjects(Map<String, String>... objectsList) {
        
        StringBuilder builder = new StringBuilder();
        
        for (Map<String, String> objects : objectsList) {
            if (objects != null) {
                for (Map.Entry<String, String> entry : objects.entrySet()) {
                    logger.debug(entry.getKey() + ": " + entry.getValue());
                    if (!entry.getValue().equals("OK")) {
                        builder.append("\t" + entry.getKey() + "\n");
                    }
                }
            }
        }
        
        return builder.toString();
    }

	/**
	 * Constroi o corpo da mensagem do email
	 * @param updateObjects            A lista com os objetos atualizados
	 * @param notUpdateObjects         A lista com os objetos nao atualizados
	 * @param notCompiledObjects       A lista com os objetos nao compilados
	 * @param notCompiledObjectsNum    A quantidade de objetos nao compilados
	 * @return
	 */
	public static String buildBodyText(String updateObjects, String notUpdateObjects, String notCompiledObjects) {
		
		StringBuilder builder = new StringBuilder();
		builder.append("OBJETOS ATUALIZADOS:" + "\n" + updateObjects + "\n");
		builder.append("OBJETOS NAO ATUALIZADOS:" + "\n"  + notUpdateObjects + "\n");
		builder.append("OBJETOS QUE NAO CONSEGUIMOS COMPILAR:" + "\n" + notCompiledObjects + "\n");
		
		return builder.toString();
	}
	
	/**
	 * Obtem o conteudo do stream
	 * @param input	O stream
	 * @return		O conteudo do stream
	 * @throws IOException
	 */
	public static String getInputStreamContents(InputStream input) throws IOException {
		
		BufferedReader buffer = null;
		StringBuilder builder = new StringBuilder();
		
		try {
    		buffer = new BufferedReader(new InputStreamReader(input));
    		while (buffer.ready()) {
    			builder.append(buffer.readLine() + "\n");
    		}
		} finally {
    		try {
    			if (buffer != null) {
    				buffer.close();
    			}
    		} catch (IOException e) {
    			logger.warn("Falha no fechamento do buffer do inputstream", e);
    		}
		}
		
		return builder.toString();
	}
	
	/**
	 * Obtem o conteudo do arquivo
	 * @param file	O arquivo
	 * @return		O conteudo do arquivo
	 * @throws IOException
	 */
	public static String getFileContents(File file) throws IOException {
		
		BufferedReader buffer = null;
		StringBuilder builder = new StringBuilder();
		
		try {
    		buffer = new BufferedReader(new InputStreamReader(new FileInputStream(file), OmuProperties.getProperty("file.encoding")));
    		while (buffer.ready()) {
    			String line = buffer.readLine();
    			if (!line.trim().equals("/")) {
    				builder.append(line + "\n");
    			}
    		}
		} finally {
    		try {
    			if (buffer != null) {
    				buffer.close();
    			}
    		} catch (IOException e) {
    			logger.warn("Falha no fechamento do buffer do arquivo: " + file.getName(), e);
    		}
		}
		
		return builder.toString();
	}
	
	/**
	 * Obtem o conteudo do arquivo do tipo Package
	 * @param file	O arquivo do tipo Package
	 * @return		O conteudo do arquivo contendo o spec e a body separados
	 * @throws IOException
	 */
	public static String[] getPackageFileContents(File file) throws IOException {
		
		BufferedReader buffer = null;
		StringBuilder specBuilder = new StringBuilder();
		StringBuilder bodyBuilder = new StringBuilder();
		String[] contents = new String[2];
		
		try {
		    buffer = new BufferedReader(new InputStreamReader(new FileInputStream(file), OmuProperties.getProperty("file.encoding")));
		
    		while (buffer.ready()) {
    			String line = buffer.readLine();
    			if (line.trim().equals("/")) {
    				break;
    			}
    			specBuilder.append(line + "\n");
    		}
    		contents[0] = specBuilder.toString();
		
    		while (buffer.ready()) {
    			String line = buffer.readLine();
    			if (line.trim().equals("/")) {
    				continue;
    			}
    			bodyBuilder.append(line + "\n");
    		}
    		contents[1] = bodyBuilder.toString();
		} finally {
    		try {
    			if (buffer != null) {
    				buffer.close();
    			}
    		} catch (IOException e) {
    			logger.warn("Falha no fechamento do buffer do arquivo: " + file.getName(), e);
    		}
		}
		
		return contents;
	}

   /**
    * Verifica se o arquivo Ddl e uma Package
    * @param ddl	O arquivo Ddl
    * @return		true se o arquivo for uma Package, false caso contrario
    * @throws IOException
    */
	public static boolean isDdlPackage(File ddl) throws IOException {
		
		BufferedReader buffer = null;
		boolean result = false;
		
		try {
    		buffer = new BufferedReader(new InputStreamReader(new FileInputStream(ddl), OmuProperties.getProperty("file.encoding")));
    		while (buffer.ready()) {
    			if (buffer.readLine().toUpperCase().contains("PACKAGE")) {
    				result = true;
    				break;
    			}
    		}
		} finally {
    		try {
    			if (buffer != null) {
    				buffer.close();
    			}
    		} catch (IOException e) {
    			logger.warn("Falha no fechamento do buffer do arquivo: " + ddl.getName(), e);
    		}
		}
		
		return result;
	}

	/**
	 * Cria uma arquivo no caminho especificado e escreve o conteudo
	 * @param filePath		O caminho onde o arquivo sera criado
	 * @param content		O conteudo a ser escrito no arquivo
	 * @return				O arquivo criado
	 * @throws IOException
	 */
	public static File writeFile(String filePath, String content) throws IOException {
		
		File file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }
        
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), OmuProperties.getProperty("file.encoding")));
            bw.write(content.replace("\n", "\r\n"));
            bw.flush();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
                logger.warn("Falha no fechamento do buffer de escrita do arquivo: " + file.getName(), e);
            }
        }
        
        return file;
	}
	
	/**
	 * Copia um arquivo para o caminho especificado
	 * @param file	O arquivo a ser copiado
	 * @param path	O caminho onde o arquivo sera copiado
	 * @throws IOException
	 */
	public static void writeFile(File file, String path) throws IOException {
        
		File newFile = new File(path, file.getName());
		if (file.isDirectory()) {
            if (!newFile.exists()) {
            	newFile.mkdirs();
            }
            for (String children : file.list()) {
            	writeFile(new File(file, children), newFile.getAbsolutePath());
            }
        } else {
        	newFile.createNewFile();
        	InputStreamReader in = null;
            OutputStreamWriter out = null;
            try {
            	in = new InputStreamReader(new FileInputStream(file), OmuProperties.getProperty("file.encoding"));
            	out = new OutputStreamWriter(new FileOutputStream(newFile), OmuProperties.getProperty("file.encoding"));
            	char[] buffer = new char[1024];
            	int len;
            	while ((len = in.read(buffer, 0, 1024)) > 0) {
            		out.write(buffer, 0, len);
            	}
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException e) {
                    logger.warn("Falha no fechamento no stream de leitura do arquivo: " + file.getName(), e);
                }
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    logger.warn("Falha no fechamento no stream de escrita do arquivo: " + file.getName(), e);
                }
            }
        }
    }

	/**
	 * Escreve o conteudo no arquivo especificado
	 * @param file			O arquivo que sera criado
	 * @param content		O conteudo a ser escrito no arquivo
	 * @throws IOException
	 */
	public static void writeFileContents(File file, String content) throws IOException {
		
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), OmuProperties.getProperty("file.encoding")));
            bw.write(content.replace("\n", "\r\n"));
            bw.flush();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
                logger.warn("Falha no fechamento do buffer de escrita do arquivo: " + file.getName(), e);
            }
        }
        
	}
	
	/**
     * Escreve o conteudo no arquivo .omu (sem encoding e/ou quebra de linha no fim do arquivo)
     * @param file          O arquivo que sera criado
     * @param content       O conteudo a ser escrito no arquivo
     * @throws IOException
     */
    public static void writeOmuFileContents(File file, String content) throws IOException {
        
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            bw.write(content.trim());
            bw.flush();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
                logger.warn("Falha no fechamento do buffer de escrita do arquivo: " + file.getName(), e);
            }
        }
        
    }
	
	/**
    * Deleta o arquivo especificado
    * @param file	O arquivo a ser deletado
    * @return		true se a delecao ocorreu com sucesso ou false caso ocorra um erro
    */
    public static boolean deleteFile(File file) {

       if (file.exists()) {
           if (file.isDirectory()) {
               String[] files = file.list();
               for (String name : files) {
                   if (!deleteFile(new File (file.getAbsolutePath(), name))) {
                       return false;
                   }
               }
               if (!file.delete()) {
                   return false;
               }
           } else {
               if (!file.delete()) {
                   return false;
               }
           }
       } else {
           return false;
       }
       
       return true;
   }

}
