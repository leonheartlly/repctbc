package br.com.algartelecom.automacaoomudba.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author mourao
 *
 */
public class OmuMail {

	private static Logger logger = LoggerFactory.getLogger(OmuMail.class);
	
	private static String host = OmuProperties.getProperty("mail.smtp.host");
	private static String port = OmuProperties.getProperty("mail.smtp.port");
	private static String recipients = OmuProperties.getProperty("mail.smtp.recipients");

	private OmuMail() {
		
	}
	
	/**
	 * Envia um email
	 * @param subject      O assunto do email
	 * @param bodyText     O corpo do email
	 * @param attachments  Os anexos do email
	 */
	public static void sendMail(String subject, String bodyText, File... attachments) {
		
		Properties properties = configureProperties();
		Session session = getSessionInstance(properties);
		try {
			logger.info("Iniciando envio de email...");
			Multipart multipart = configureMultipart(bodyText, attachments);
			Message message = configureMessage(session, subject);
			message.setContent(multipart);
			Transport.send(message);
			logger.info("Fim do envio de email");
		} catch (AddressException e) {
		    logger.error("Falha no envio do email", e);
		} catch (MessagingException e) {
		    logger.error("Falha no envio do email", e);
		} catch (UnsupportedEncodingException e) {
		    logger.error("Falha no envio do email", e);
        }
		
	}

	/**
	 * Configura as propriedades para o envio de email
	 * @return	As propriedades configuradas
	 */
	private static Properties configureProperties() {
		
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port",port);
		
		return properties;
	}

	/**
	 * Obtem uma instancia de sessao para o envio de email
	 * @param properties	As propriedades configuradas
	 * @return				Uma instancia de sessao pra o envio de email
	 */
	private static Session getSessionInstance(Properties properties) {
	    return Session.getDefaultInstance(properties, null);
	}

	/**
	 * Configura a mensagem do email
	 * @param session	A sessao que sera enviado o email
	 * @param subject	O assunto do email
	 * @return			A mensagem configurada
	 * @throws AddressException
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException 
	 */
	private static Message configureMessage(Session session, String subject) throws AddressException, MessagingException, UnsupportedEncodingException {

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(OmuProperties.getProperty("mail.smtp.from")));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
		message.setSubject(subject + " " + new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		
		return message;
	}

	/**
	 * Configura o texto e os anexos do email
	 * @param bodyText		O texto do email
	 * @param attachments	Os arquivos anexados ao email
	 * @return				Os elementos configurados
	 * @throws MessagingException
	 */
	private static Multipart configureMultipart(String bodyText, File[] attachments) throws MessagingException {

		MimeBodyPart messageAttachment = new MimeBodyPart();
		messageAttachment.setContent(bodyText, "text/plain; charset=UTF-8");
		
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageAttachment);
		
		for (File attachment : attachments) {
			
			if (attachment != null && attachment.isFile()) {
				
				MimeBodyPart mimeBodyPart = new MimeBodyPart();
				mimeBodyPart.setDataHandler(new DataHandler(new FileDataSource(attachment)));
				mimeBodyPart.setFileName(attachment.getName());
				multipart.addBodyPart(mimeBodyPart);
			}
		}
		
		return multipart;
	}

}
