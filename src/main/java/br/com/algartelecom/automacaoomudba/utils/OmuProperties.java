package br.com.algartelecom.automacaoomudba.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.slf4j.LoggerFactory;

/**
 * 
 * @author mourao
 *
 */
public class OmuProperties {

	/**
	 * Obtem o valor da propriedade com a chave especificada
	 * @param propertyKey	A chave da propriedade
	 * @return				O valor da propriedade
	 */
	public static String getProperty(String propertyKey) {
		
		InputStream inputStream = null; 
		Properties properties = null;
		
		try {
			// TODO - remover poc e teste
			inputStream = OmuProperties.class.getClassLoader().getResourceAsStream("omu.properties");
//			inputStream = OmuProperties.class.getClassLoader().getResourceAsStream("poc.properties");
//		    inputStream = OmuProperties.class.getClassLoader().getResourceAsStream("teste.properties");
			properties = new Properties();
			properties.load(inputStream);
		} catch (IOException e) {
			LoggerFactory.getLogger(OmuProperties.class).error("Falha na leitura do arquivo de propriedades");
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				LoggerFactory.getLogger(OmuProperties.class).warn("Falha no fechamento do stream do arquivo de propriedades", e);
			}
		}
		
		return properties.getProperty(propertyKey);
	}
	
	public static String getPeopleOraclePassword(String base) throws IOException, InterruptedException {
		
	    BufferedReader reader = null;
        String password = null;
        int result = -1;
        
        try {
            
            Process process = Runtime.getRuntime().exec("./lib/getPass.sh " + getProperty("people.oracle."+base+".env"));
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            StringBuilder cmdOut = new StringBuilder();
            String lineOut = null;
            while ((lineOut = reader.readLine()) != null) {  
                cmdOut.append("\t" + lineOut + "\n");  
            }
            result = process.waitFor();
            
            if (result == 0) {
                password = cmdOut.toString().trim();
            }
            
            return password;
            
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    LoggerFactory.getLogger(OmuProperties.class).warn("Falha no fechamento do buffer de leitura");
                }
            }
        }
	}

	public static String getVantiveOraclePassword(String base) throws IOException, InterruptedException {
		
	    BufferedReader reader = null;
        String password = null;
        int result = -1;
        
        try {
            
            Process process = Runtime.getRuntime().exec("./lib/getPass.sh " + getProperty("vantive.oracle."+base+".env"));
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            StringBuilder cmdOut = new StringBuilder();
            String lineOut = null;
            while ((lineOut = reader.readLine()) != null) {  
                cmdOut.append("\t" + lineOut + "\n");  
            }
            result = process.waitFor();
            
            if (result == 0) {
                password = cmdOut.toString().trim();
            }
            
            return password;
            
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    LoggerFactory.getLogger(OmuProperties.class).warn("Falha no fechamento do buffer de leitura");
                }
            }
        }
	}
	
}
