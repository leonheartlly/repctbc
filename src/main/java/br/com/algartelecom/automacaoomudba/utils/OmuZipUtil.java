package br.com.algartelecom.automacaoomudba.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.algartelecom.automacaoomudba.omu.OmuException;

/**
 * 
 * @author mourao
 *
 */
public class OmuZipUtil {

    private static final int BUFFER_SIZE = 2048;
    private static BufferedInputStream bufferedInputStream;

    private static Logger logger = LoggerFactory.getLogger(OmuZipUtil.class);

    private OmuZipUtil() {
    }

    /**
     * Extrai um arquivo zip no caminho especificado
     * @param file	O arquivo a ser extraido
     * @param path	O caminho onde o arquivo sera extraido
     * @return		true se o arquivo foi extraido com sucesso, false caso contrario
     */
    public static boolean extractFile(File file, String path) {

        boolean result = false;
        ZipFile zipFile = null;
		
		try {
			zipFile = new ZipFile(file);
			@SuppressWarnings("unchecked")
			Enumeration<ZipEntry> entries = (Enumeration<ZipEntry>) zipFile.entries();
			while(entries.hasMoreElements()) {
				extractElement(zipFile, entries.nextElement(), path);
			}
			result = true;
			logger.info("Arquivo " + file.getName() + " extraido com sucesso.");
		} catch (ZipException e) {
			logger.error("Falha na leitura no arquivo zip: " + file.getName(), e);
		} catch (IOException e) {
			logger.error("Falha na leitura/escrita do arquivo: " + file.getName(), e);
		} finally {
			if (zipFile != null) {
				try {
					zipFile.close();
				} catch (IOException e) {
					logger.warn("Falha no fechamento do arquivo zip: " + file.getName(), e);
				}
			}
		}
		
		return result;
	}

	/**
	 * Extrai um elemento do arquivo zip
	 * @param zipFile		O arquivo zip
	 * @param element		O elemento do arquivo zip
	 * @param path			O camino onde o elemento sera extraido
	 * @throws IOException
	 */
	private static void extractElement(ZipFile zipFile, ZipEntry element, String path) throws IOException {

		ZipEntry entry = element;
		File file = new File(path, entry.getName());
		
        if (entry.isDirectory()) {
        	file.mkdirs();
        } else if(entry.getName().toLowerCase().endsWith(".zip")) {
        	extractFile(file, file.getAbsolutePath());
        } else {
        	if (!file.getParentFile().exists()) {
            	file.getParentFile().mkdirs();
            } 
            writeElement(zipFile, file, entry);
        }
	}

	/**
	 * Escreve uma entrada do arquivo zip no disco
	 * @param zipFile		O arquivo zip
	 * @param file			O arquivo no disco que representa a entrada
	 * @param entry			A entrada do arquivo zip
	 * @throws IOException
	 */
	private static void writeElement(ZipFile zipFile, File file, ZipEntry entry) throws IOException {

		InputStreamReader input = null;
		OutputStreamWriter output = null;
        
        try {
        	input = new InputStreamReader(zipFile.getInputStream(entry), "windows-1252");
        	output = new OutputStreamWriter(new FileOutputStream(file), "windows-1252");
        	char[] buffer = new char[BUFFER_SIZE];
        	int len;
        	while ((len = input.read(buffer, 0, BUFFER_SIZE)) > 0) {
        		output.write(buffer, 0, len);
        	}
        	logger.info("\t Arquivo " + file.getName() + " extraido");
        } finally {
            try {
            	if (input != null) {
            		input.close();
            	}
            } catch (IOException e) {
            	logger.warn("Falha no fechamento do stream de entrada da extracao do arquivo zip: " + file.getName(), e);
            }
            try {
            	if (output != null) {
            		output.close();
            	}
            } catch (IOException e) {
            	logger.warn("Falha no fechamento do stream de saida da extracao do arquivo zip: " + file.getName(), e);
            }
        }
	}
	
	/**
	 * Comprime os arquivos gerado pelo processo.
	 * @param file             O arquivo a ser comprimido
	 * @param destinationPath  O diretorio de destino
	 * @return                 O arquivo comprimido
	 * @throws OmuException 
	 */
    public static File compress(File file, String destinationPath) throws OmuException {

        ZipOutputStream zipOutputStream = null;
        File zipFile = new File(destinationPath, file.getName() + ".zip");
        
        try {
            logger.info("Iniciando compactacao do arquivo: " + file.getName());
            zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));
            
            if (file.isDirectory()) {
                addFolderToZip(file, file.getName(), zipOutputStream);
            } else {
                addFileToZip(file, zipOutputStream);
            }
            
            zipOutputStream.flush();
            logger.info("Fim da compactacao do arquivo.");
        } catch (IOException e) {
            throw new OmuException("", e);
        } finally {
            if (zipOutputStream != null) {
                try {
                    zipOutputStream.close();
                } catch(IOException e) {
                    logger.warn("Falha no fechamento do zip stream do arquivo: " + file.getAbsolutePath(), e);
                }
            }
        }
        
        return zipFile;
    }
    
    /**
     * Adiciona um diretorio a saida do zip stream corrente.
     * @param folder            O diretorio que sera adicionado
     * @param parentFolder      O caminho do diretorio pai
     * @param zipOutputStream   O zip stream corrente
     * @throws IOException
     */
    private static void addFolderToZip(File folder, String parentFolder, ZipOutputStream zipOutputStream) throws IOException {
        
        for (File file : folder.listFiles()) {
            
            String filepath = new File(parentFolder, file.getName()).getPath();
            
            if (file.isDirectory()) {
                addFolderToZip(file, filepath, zipOutputStream);
                continue;
            }
            
            zipOutputStream.putNextEntry(new ZipEntry(filepath));
            bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            
            byte[] bytesIn = new byte[BUFFER_SIZE];
            int read = 0;
            
            while ((read = bufferedInputStream.read(bytesIn)) != -1) {
                zipOutputStream.write(bytesIn, 0, read);
            }
            zipOutputStream.closeEntry();
        }
    }

    /**
     * Adiciona um arquivo a saida do zip stream corrente.
     * @param file              O arquivo a ser adicionado
     * @param zipOutputStream   O zip stream corrente
     * @throws IOException
     */
    private static void addFileToZip(File file, ZipOutputStream zipOutputStream) throws IOException {
        
        zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
        bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
        
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
 
        while ((read = bufferedInputStream.read(bytesIn)) != -1) {
            zipOutputStream.write(bytesIn, 0, read);
        }
        zipOutputStream.closeEntry();
    }
	
}
