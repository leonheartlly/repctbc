package br.com.algartelecom.automacaoomudba.omu.packages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FileOmuContents {

	private List<String> objects;
	private List<String> forms;
	private List<String> rules;
	private List<String> codes;
	private List<String> tables;
	private List<String> vapps;
	private List<String> groups;
	private List<String> screens;
	private List<String> actives;
	private List<String> reports;
	
	public FileOmuContents() {
		super();
		objects = new ArrayList<String>();
		forms = new ArrayList<String>();
		rules = new ArrayList<String>();
		codes = new ArrayList<String>();
		tables = new ArrayList<String>();
		vapps = new ArrayList<String>();
		groups = new ArrayList<String>();
		screens = new ArrayList<String>();
		actives = new ArrayList<String>();
		reports = new ArrayList<String>();
	}
	
	public FileOmuContents(File omuFile) throws IOException {
		this();
		setFileOMUContents(omuFile);
	}
	
	public List<String> getObjects() {
		return objects;
	}

	public List<String> getForms() {
		return forms;
	}

	public List<String> getRules() {
		return rules;
	}

	public List<String> getCodes() {
		return codes;
	}

	public List<String> getTables() {
		return tables;
	}

	public List<String> getVapps() {
		return vapps;
	}

	public List<String> getGroups() {
		return groups;
	}

	public List<String> getScreens() {
		return screens;
	}

	public List<String> getActive() {
		return actives;
	}

	public List<String> getReport() {
		return reports;
	}

	/**
	 * Retorna uma lista com o conteudo do arquivo .omu
	 * @return	A lista com o conteudo do arquivo .omu
	 */
	public List<String> getFileOMUContentsList() {
		List<String> contents = new ArrayList<String>();
		contents.addAll(objects);
		contents.addAll(forms);
		contents.addAll(rules);
		contents.addAll(codes);
		contents.addAll(tables);
		contents.addAll(vapps);
		contents.addAll(groups);
		contents.addAll(screens);
		contents.addAll(actives);
		contents.addAll(reports);
		return contents;
	}
	
	/**
	 * Retorna o conteudo do arquivo .omu
	 * @return O conteudo do arquivo .omu
	 */
	public String getFileOMUContents() {
		StringBuilder contents = new StringBuilder();
		
		if (!objects.isEmpty()) {
			contents.append("OBJ_BEGIN\n");
			for (String object : objects) {
				contents.append(object+"\n");
			}
			contents.append("OBJ_END\n");
		}
		if (!forms.isEmpty()) {
			contents.append("FRM_BEGIN\n");
			for (String form : forms) {
				contents.append(form+"\n");
			}
			contents.append("FRM_END\n");
		}
		if (!rules.isEmpty()) {
			contents.append("RUL_BEGIN\n");
			for (String rule : rules) {
				contents.append(rule+"\n");
			}
			contents.append("RUL_END\n");
		}
		if (!codes.isEmpty()) {
			contents.append("COD_BEGIN\n");
			for (String code : codes) {
				contents.append(code+"\n");
			}
			contents.append("COD_END\n");
		}
		if (!tables.isEmpty()) {
			contents.append("TAB_BEGIN\n");
			for (String table : tables) {
				contents.append(table+"\n");
			}
			contents.append("TAB_END\n");
		}
		if (!vapps.isEmpty()) {
			contents.append("VBA_BEGIN\n");
			for (String vapp : vapps) {
				contents.append(vapp+"\n");
			}
			contents.append("VBA_END\n");
		}
		if (!groups.isEmpty()) {
			contents.append("GRP_BEGIN\n");
			for (String group : groups) {
				contents.append(group+"\n");
			}
			contents.append("GRP_END\n");
		}
		if (!screens.isEmpty()) {
			contents.append("SRC_BEGIN\n");
			for (String screen : screens) {
				contents.append(screen+"\n");
			}
			contents.append("SRC_END\n");
		}
		if (!actives.isEmpty()) {
			contents.append("OCX_BEGIN\n");
			for (String active : actives) {
				contents.append(active+"\n");
			}
			contents.append("OCX_END\n");
		}
		if (!reports.isEmpty()) {
			contents.append("RPT_BEGIN\n");
			for (String report : reports) {
				contents.append(report+"\n");
			}
			contents.append("RPT_END\n");
		}
		
		return contents.toString().substring(0, contents.length());
	}
	
	/**
	 * Escreve o conteudo no arquivo .omu
	 * @param omuFile	O arquivo .omu
	 * @throws IOException 
	 */
	public void setFileOMUContents(File omuFile) throws IOException {

		if (null != omuFile) {
			
			BufferedReader buffer = new BufferedReader(new InputStreamReader(new FileInputStream(omuFile), "ISO-8859-1"));
			
			while (buffer.ready()) {
				String line = buffer.readLine();
				
				if (line.equals("OBJ_BEGIN")) {
					setContents(buffer, objects, "OBJ_END");
				} else if (line.equals("FRM_BEGIN")) {
					setContents(buffer, forms, "FRM_END");
				} else if (line.equals("RUL_BEGIN")) {
					setContents(buffer, rules, "RUL_END");
				} else if (line.equals("COD_BEGIN")) {
					setContents(buffer, codes, "COD_END");
				} else if (line.equals("TAB_BEGIN")) {
					setContents(buffer, tables, "TAB_END");
				} else if (line.equals("VBA_BEGIN")) {
					setContents(buffer, vapps, "VBA_END");
				} else if (line.equals("GRP_BEGIN")) {
					setContents(buffer, groups, "GRP_END");
				} else if (line.equals("SRC_BEGIN")) {
					setContents(buffer, screens, "SRC_END");
				} else if (line.equals("OCX_BEGIN")) {
					setContents(buffer, actives, "OCX_END");
				} else if (line.equals("RPT_BEGIN")) {
					setContents(buffer, reports, "RPT_END");
				}
			}
			
			buffer.close();
		}
	}
	
	/**
	 * Obtem o conteudo especifico do arquivo .omu e armazena em uma lista
	 * @param buffer		O conteudo do arquivo .omu
	 * @param contentsList	A lista onde o conteudo sera armazenado
	 * @param breakLine		O ponto de parada do conteudo
	 * @throws IOException
	 */
	private void setContents(BufferedReader buffer, List<String> contentsList, String breakLine) throws IOException {
		String line;
		while (buffer.ready()) {
			line = buffer.readLine();
			if (line.equals(breakLine)) {
				break;
			} else {
				contentsList.add(line);
			}
		}
	}
	
	/**
	 * Adiciona o conteudo de um arquivo .omu ao arquivo .omu atual
	 * @param fileOMUContents	O arquivo .omu a ser adicionado ao arquivo .omu atual
	 */
	public void mergeFileOMUFileContents(FileOmuContents fileOMUContents) {
		mergeContents(objects, fileOMUContents.getObjects());
		mergeContents(forms, fileOMUContents.getForms());
		mergeContents(rules, fileOMUContents.getRules());
		mergeContents(codes, fileOMUContents.getCodes());
		mergeContents(tables, fileOMUContents.getTables());
		mergeContents(vapps, fileOMUContents.getVapps());
		mergeContents(groups, fileOMUContents.getGroups());
		mergeContents(screens, fileOMUContents.getScreens());
		mergeContents(actives, fileOMUContents.getActive());
		mergeContents(reports, fileOMUContents.getReport());
	}
	
	/**
	 * Adiciona o conteudo de uma item do .omu em outro
	 * @param contentsToMerge	O item do .omu alvo
	 * @param contentsFromMerge	O item do .omu copiado
	 */
	private void mergeContents(List<String> contentsToMerge, List<String> contentsFromMerge) {
		for (String content : contentsFromMerge) {
			contentsToMerge.add(content);
		}
	}
	
}
