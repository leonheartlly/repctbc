package br.com.algartelecom.automacaoomudba.omu.packages;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Representa um dos diretorios contidos no pacote da demanda (DDLs_Vantive, DDLs_People, Scripts_Vantive, Scripts_People)
 * @author mourao
 *
 */
public class FilesDir {

	private List<File> fileList;
	private List<String> filenamesList;

	public FilesDir() {
		super();
		fileList = new ArrayList<File>();
		filenamesList = new ArrayList<String>();
	}

	public FilesDir(File fileDir) {
		this();
		fillFileList(fileDir);
	}
	
	public List<File> getFileList() {
		return fileList;
	}

	public List<String> getFilenamesList() {
		return filenamesList;
	}

	public void setFileList(List<File> fileList) {
		this.fileList = fileList;
	}
	
	public void setFilenamesList(List<String> filenamesList) {
		this.filenamesList = filenamesList;
	}

	/**
	 * Obtem os arquivos contidos no diretorio
	 * @param fileDir	O diretorio que contem os arquivos
	 */
	public void fillFileList(File fileDir) {
		
		if (null != fileDir) {
			String[] filenames = fileDir.list();
			for (String filename : filenames) {
				fileList.add(new File(fileDir.getAbsolutePath(), filename));
				filenamesList.add(filename);
			}
		}
	}

	/**
	 * Adiciona os arquivos de um diretorio ao diretorio atual 
	 * @param filesDir	O diretorio a ser adicionado ao diretorio atual
	 */
	public void mergeFilesDir(FilesDir filesDir) {

		for (File fileToAdd : filesDir.getFileList()) {
			if (!filenamesList.contains(fileToAdd.getName())) {
				fileList.add(fileToAdd);
				filenamesList.add(fileToAdd.getName());
			}
		}
	}
	
	/**
	 * Ordena a lista de arquivos em ordem numerica crescente
	 */
	public void sortFileList() {
		
		Collections.sort(fileList, new Comparator<File>() {
			@Override
			public int compare(File file1, File file2) {
			    String[] splitFile1 = file1.getName().split("_");
			    String[] splitFile2 = file2.getName().split("_");
			    int numFile1 = Integer.parseInt(splitFile1[0]);
			    int numFile2 = Integer.parseInt(splitFile2[0]);
			    return Integer.compare(numFile1, numFile2);
			}
		});
	}

	/**
	 * Checa se a lista de objetos é vazia
	 * @return true se a lista for vazia, false caso contrario
	 */
    public boolean isEmpty() {
        return fileList.isEmpty();
    }
	
}
