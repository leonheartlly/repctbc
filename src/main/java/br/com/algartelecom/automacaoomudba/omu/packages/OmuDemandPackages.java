package br.com.algartelecom.automacaoomudba.omu.packages;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.algartelecom.automacaoomudba.utils.OmuUtil;

/**
 * Representa os pacotes vindo das demandas para o processo OMU
 * @author mourao
 *
 */
public class OmuDemandPackages {

	private List<OmuPackage> packagesList;
	
	public OmuDemandPackages() {
		super();
		packagesList = new ArrayList<OmuPackage>();
	}
	
	public List<OmuPackage> getPackagesList() {
		return packagesList;
	}
	
	/**
	 * Le os pacotes das demandas
	 * @param packagesFileList	A lista de pacotes
	 * @throws IOException
	 */
	public void readPackages(List<File> packagesFileList) throws IOException {
		
		for (File packageFile : packagesFileList) {
			packagesList.add(OmuUtil.createOmuDemandPackage(packageFile));
		}
	}
	
}
