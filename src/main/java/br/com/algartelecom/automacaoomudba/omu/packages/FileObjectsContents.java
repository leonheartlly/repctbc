package br.com.algartelecom.automacaoomudba.omu.packages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FileObjectsContents {

	private List<String> vantiveObjects;
	private List<String> peopleObjects;
	
	public FileObjectsContents() {
		super();
		vantiveObjects = new ArrayList<String>();
		peopleObjects = new ArrayList<String>();
	}
	
	public FileObjectsContents(File objectsFile) throws IOException {
		this();
		setFileObjectsContents(objectsFile);
	}
	
	public List<String> getVantiveObjects() {
		return vantiveObjects;
	}

	public List<String> getPeopleObjects() {
		return peopleObjects;
	}

	/**
	 * Retorna uma lista do conteudo do arquivo de resultado
	 * @return	Uma lista com conteudo do arquivo de resultado
	 */
	public List<String> getFileObjectsContentsList() {
		List<String> contents = new ArrayList<String>();
		contents.addAll(vantiveObjects);
		contents.addAll(peopleObjects);
		return contents;
	}
	
	/**
	 * Retorna o conteudo do arquivo de resultado
	 * @return	O conteudo do arquivo de resultado
	 */
	public String getFileObjectsContents() {
		
		StringBuilder contents = new StringBuilder();
		
		for (String object : vantiveObjects) {
			contents.append(object + "\n");
		}
		for (String object : peopleObjects) {
			contents.append(object + "\n");
		}
		
		return contents.toString();
	}
	
	/**
	 * Obtem o conteudo do arquivo de resultado
	 * @param objectsFile	O arquivo de resultado
	 * @throws IOException 
	 */
	public void setFileObjectsContents(File objectsFile) throws IOException {
		
		if (null != objectsFile) {
			
			BufferedReader buffer = new BufferedReader(new InputStreamReader(new FileInputStream(objectsFile)));
			
			while (buffer.ready()) {
				String line = buffer.readLine();
				if (line.startsWith("VANTIVE")) {
					vantiveObjects.add(line);
				} else if (line.startsWith("PEOPLE")) {
					peopleObjects.add(line);
				}
			}
			
			buffer.close();
		}
	}
	
	/**
	 * Adiciona o conteudo de um arquivo de resultado ao arquivo de resultado atual
	 * @param fileObjects	O arquivo de resultado a ser adicionado ao arquivo de resultado atual
	 */
	public void mergeFileObjectsContents(FileObjectsContents fileObjectsContents) {
		
		mergeObjects(vantiveObjects, fileObjectsContents.getVantiveObjects());
		mergeObjects(peopleObjects, fileObjectsContents.getPeopleObjects());
	}
	
	/**
	 * Adiciona o conteudo de uma lista de objetos em outra
	 * @param objectsToMerge	A lista de objetos alvo
	 * @param objectsFromMerge	A lista de objetos copiada
	 */
	private void mergeObjects(List<String> objectsToMerge, List<String> objectsFromMerge) {
		
		for (String content : objectsFromMerge) {
			if (!objectsToMerge.contains(content)) {
				objectsToMerge.add(content);
			}
		}
	}
	
}
