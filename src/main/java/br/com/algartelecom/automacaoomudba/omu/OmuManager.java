package br.com.algartelecom.automacaoomudba.omu;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.algartelecom.automacaoomudba.connection.oracle.OracleConnectionPeople;
import br.com.algartelecom.automacaoomudba.connection.oracle.OracleConnectionVantive;
import br.com.algartelecom.automacaoomudba.omu.packages.FilesDir;
import br.com.algartelecom.automacaoomudba.omu.packages.OmuDemandPackages;
import br.com.algartelecom.automacaoomudba.omu.packages.OmuPackage;
import br.com.algartelecom.automacaoomudba.utils.OmuProperties;
import br.com.algartelecom.automacaoomudba.utils.OmuUtil;
import br.com.algartelecom.automacaoomudba.utils.OmuZipUtil;

/**
 * Contem metodos para a execucao do processo OMU
 * @author mourao
 *
 */
public class OmuManager {

	private static Logger logger = LoggerFactory.getLogger(OmuManager.class);
	
	private OmuPackage omuPackage;
	
	public OmuManager() {
		super();
		omuPackage = new OmuPackage();
	}
	
	public OmuPackage getOmuPackage() {
		return omuPackage;
	}
	
	public Map<String, String> getOmuVantiveResult(String result) {
	    
	    Map<String, String> omuResult = new HashMap<String, String>();
	    for (String object : omuPackage.getOmuFile().getFileOMUContentsList()) {
	        omuResult.put(object, result);
	    }
	    
	    return omuResult;
	}
	
	/**
	 * Extrai as demandas para o diretorio especificado
	 * @param demandsDirPath	O caminho do diretorio das demandas
	 * @param path				O caminho do diretorio onde as demandas serao extraidas
	 */
	public boolean extractDemands(String demandsDirPath, String path) {
		
	    boolean result = true;
		File demandsDir = new File(demandsDirPath);
		List<String> demandsDirList = Arrays.asList(demandsDir.list());
		
		if (demandsDirList.isEmpty()) {
		    result = false;
		    logger.info("Nao ha demandas a serem executadas");
		} else {
		    logger.info("Iniciando extracao dos arquivos das demandas...");
		    for (String filename : demandsDir.list()) {
                File newFile = new File(demandsDirPath, filename);
                OmuZipUtil.extractFile(newFile, path);
            }
		    logger.info("Aquivos das demandas extraidos");
		}
		
		return result;
	}

	/**
	 * Cria um pacote para o processo OMU a partir de varias demandas
	 * @param packagesDirPath  O caminho do diretorio que contem as demandas
	 * @param path             O caminho onde o pacote sera escrito
	 * @param basename         O nome da base
	 * @param demand           O tipo do processo OMU
	 * @return                 O arquivo do pacote
	 * @throws OmuException
	 */
	public File createOmuPackageToProcess(String packagesDirPath, String path, String basename, String demand) throws OmuException {
		
		File file = null;
		try {
			List<OmuPackage> omuPackages = addDemandPackages(packagesDirPath);
			for (OmuPackage demandPackage : omuPackages) {
				omuPackage.mergePackage(demandPackage);
			}
			file = omuPackage.writePackage(path, basename, demand);
			logger.info("Pacote do processo OMU criado com sucesso");
		} catch (IOException e) {
			throw new OmuException("Falha na criacao do pacote do processo OMU", e);
		}
		
		return file;
	}

	/**
	 * Envia um arquivo para o servidor
	 * @param file     O arquivo a ser enviado
	 * @return         0 se o arquivo foi enviado com sucesso, 1 caso contrario
	 * @throws OmuException
	 */
	public synchronized int sendFile(File file) throws OmuException {
		
	    BufferedReader reader = null;
	    int result = -1;
	    
	    try {
	        
	        logger.info("Enviando arquivo " + file.getName() + " para o servidor...");
            Process process = Runtime.getRuntime().exec("./lib/scpCommand.sh " + file.getAbsolutePath());
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            StringBuilder cmdOut = new StringBuilder();
            String lineOut = null;
            while ((lineOut = reader.readLine()) != null) {  
                cmdOut.append("\t" + lineOut + "\n");  
            }
            result = process.waitFor();
            logger.info(cmdOut.toString().trim());
            
            return result;
            
        } catch (IOException e) {
            throw new OmuException("", e);
        } catch (InterruptedException e) {
            throw new OmuException("", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.warn("Falha no fechamento do buffer de leitura");
                }
            }
        }
	    
	}
	
	/**
	 * Executa o export/import do arquivo .omu
	 * @param sourceServer     O servidor de origem/destino
	 * @param database         O nome da instancia do banco
	 * @param mode             O modo de execucao (export/import)
	 * @param schemaFile       O arquivo com os objetos
	 * @return                 0 se o processo ocorreu com sucesso, 1 caso contrario
	 * @throws OmuException
	 */
	public int executeOmu(String sourceServer, String database, String mode, String schemaFile) throws OmuException {
		
	    BufferedReader reader = null;
	    int result = -1;
	    
	    try {
            Process process = Runtime.getRuntime().exec("./lib/sshCommand.sh " + sourceServer + " " + database + " " + mode + " " + schemaFile);
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            StringBuilder cmdOut = new StringBuilder();
            String lineOut = null;
            while ((lineOut = reader.readLine()) != null) {  
                cmdOut.append("\t" + lineOut + "\n");  
            }
            
            result = process.waitFor();
            logger.info(cmdOut.toString());
            
            return result;
            
        } catch (IOException e) {
            throw new OmuException("", e);
        } catch (InterruptedException e) {
            throw new OmuException("", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.warn("Falha no fechamento do buffer de leitura");
                }
            }
        }
	    
	}

	/**
	 * Compila os objetos invalidos no banco People
	 * @param base	O tipo da base
	 * @return		Os objetos que nao foram compilados
	 * @throws OmuException 
	 */
	public String compilePeopleObjects(String base) throws OmuException {
		
	    String peopleCompileResult = null;
        BufferedReader reader = null;
        int result = -1;
        
        try {
            
            logger.info("Iniciando compilacao dos objetos invalidos People...");
            
            Process process = Runtime.getRuntime().exec("./lib/compila.sh " 
                    + OmuProperties.getProperty("people.oracle."+base+".env") + " " 
                    + OmuProperties.getProperty("people.oracle."+base+".host"));
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            StringBuilder cmdOut = new StringBuilder();
            String lineOut = null;
            while ((lineOut = reader.readLine()) != null) {  
                cmdOut.append("\t" + lineOut + "\n");  
            }
            
            result = process.waitFor();
            peopleCompileResult = cmdOut.toString();
            logger.info(result+"");
            logger.info(peopleCompileResult);
                        
            logger.info("Fim da compilacao dos objetos People");
            
        } catch (IOException e) {
            throw new OmuException("", e);
        } catch (InterruptedException e) {
            throw new OmuException("", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.warn("Falha no fechamento do buffer de leitura");
                }
            }
        }
        
		return peopleCompileResult;
	}
	
	/**
	 * Compila os objetos invalidos no banco Vantive
	 * @param base	O tipo da base
	 * @return		Os objetos que nao foram compilados
	 * @throws OmuException 
	 */
	public String compileVantiveObjects(String base) throws OmuException {
		
		String vantiveCompileResult = null;
        BufferedReader reader = null;
        int result = -1;
        
        try {
            
            logger.info("Iniciando compilacao dos objetos invalidos Vantive...");
            
            Process process = Runtime.getRuntime().exec("./lib/compila.sh " 
                    + OmuProperties.getProperty("vantive.oracle."+base+".env") + " " 
                    + OmuProperties.getProperty("vantive.oracle."+base+".host"));
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            StringBuilder cmdOut = new StringBuilder();
            String lineOut = null;
            while ((lineOut = reader.readLine()) != null) {  
                cmdOut.append("\t" + lineOut + "\n");  
            }
            
            result = process.waitFor();
            vantiveCompileResult = cmdOut.toString();
            logger.info(result+"");
            logger.info(vantiveCompileResult);
                        
            logger.info("Fim da compilacao dos objetos Vantive");
            
        } catch (IOException e) {
            throw new OmuException("", e);
        } catch (InterruptedException e) {
            throw new OmuException("", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.warn("Falha no fechamento do buffer de leitura");
                }
            }
        }
        
        return vantiveCompileResult;
	}

	/**
	 * Atualiza os projetos People na base correspondente
	 * @param base O tipo da base
	 * @return     A lista com o nome dos projetos People que falharam
	 * @throws OmuException 
	 */
	public Map<String, String> updatePeopleProjects(String originBase, String destinationBase) throws OmuException {
		
	    logger.info("Iniciando execucao dos Projetos People...");
	    Map<String, String> projectsResult = new HashMap<String, String>();
		List<String> peopleProjects = getPeopleProjectNames();
		
		for (String projeto : peopleProjects) {
		    
		    Socket client = null;
		    PrintStream output = null;
		    InputStreamReader input = null;
		    
			try {
			    logger.info("Atualizando projeto People: " + projeto);
    		    client = new Socket(OmuProperties.getProperty("socket.server.host"), Integer.parseInt(OmuProperties.getProperty("socket.server.port")));
    			output = new PrintStream(client.getOutputStream());
    			input = new InputStreamReader(client.getInputStream());
    			
    			// Envia os dados do projeto para o listener
    			output.println("import" + ";" + OmuProperties.getProperty("people.oracle."+originBase+".env") 
    			        + ";" + OmuProperties.getProperty("people.oracle."+destinationBase+".env") 
    			        + ";" + OmuProperties.getProperty("people.client.username."+originBase) 
    			        + ";" + OmuProperties.getProperty("people.client.password."+destinationBase) 
    			        + ";" + projeto);
    			
    			char[] buffer = new char[2048];
                while (input.read(buffer) > 0) {
                }
    			
    			String reply = new String(buffer).trim();
    			logger.debug(reply);
    			if (reply.equals("OK")) {
    				logger.info(projeto + " atualizado com sucesso");
    				projectsResult.put(projeto, "OK");
    			} else if (reply.equals("EXCEPTION")) {
    			    logger.error(projeto + " nao atualizado");
    			    projectsResult.put(projeto, "NO");
    			}
    			
			} catch (IOException e) {
			    throw new OmuException("", e);
            } finally {
			    if (input != null) {
			        try {
                        input.close();
                    } catch (IOException e) {
                        logger.warn("Falha no fechamento do stream People", e);
                    }
			    }
			    if (output != null) {
			        output.close();
			    }
                if (client != null) {
                    try {
                        client.close();
                    } catch (IOException e) {
                        logger.warn("Falha no fechamento do socket para execucao dos Projetos People", e);
                    }
                }
			}
		}
		logger.info("Fim da execucao dos Projetos People");
		
		return projectsResult;
	}
	
	/**
	 * Executa os scripts People na base correspondente
	 * @param base	O tipo da base
	 * @return		O conjunto com o nome dos scripts People que falharam
	 * @throws OmuException 
	 */
	public Map<String, String> runPeopleScripts(String base) {
		
		Connection peopleConn = null;
		Map<String, String> scriptsResult = null;
		FilesDir scriptsPeople = omuPackage.getScriptsPeople();
		
		try {
		    if (!scriptsPeople.isEmpty()) {
		        logger.info("Inicio da execucao dos Scripts People...");
	            peopleConn = new OracleConnectionPeople(base).getConnection();
	            scriptsPeople.sortFileList();
	            scriptsResult = executeScriptsFiles(peopleConn, scriptsPeople);
	            logger.info("Scripts People executados");
		    } else {
		        logger.info("Nao ha Scripts People a serem executados");
		    }
		} catch (SQLException e) {
		    logger.error("Falha na abertura da conexao People para execucao dos scripts");
            logger.debug("", e);
		} catch (IOException e) {
            logger.debug("", e);
        } catch (InterruptedException e) {
            logger.debug("", e);
        } finally {
			try {
				if (peopleConn != null) {
					peopleConn.close();
				}
			} catch (SQLException e) {
				logger.warn("Falha no fechamento da conexao People para execucao dos scripts", e);
			}
		}
		
		return scriptsResult;
	}
	
	/**
	 * Executa as ddls People na base correspondente
	 * @param base	O tipo da base
	 * @return		O conjunto com o nome das ddls People que falharam
	 * @throws OmuException 
	 */
	public Map<String, String> runPeopleDdls(String base) {
		
		Connection peopleConn = null;
		Map<String, String> ddlsResult = null;
		FilesDir ddlsPeople = omuPackage.getDdlsPeople();
		
		try {
		    if (!ddlsPeople.isEmpty()) {
    			logger.info("Inicio da execucao das Ddls People...");
    			peopleConn = new OracleConnectionPeople(base).getConnection();
    			ddlsResult = executeDdlFiles(peopleConn, ddlsPeople);
    			logger.info("Ddls People executadas");
		    } else {
		        logger.info("Nao ha ddls People a serem executadas");
		    }
		} catch (SQLException e) {
		    logger.error("Falha na abertura da conexao People para execucao das ddls");
            logger.debug("", e);
		} catch (IOException e) {
		    logger.debug("", e);
        } catch (InterruptedException e) {
            logger.debug("", e);
        } finally {
			try {
				if (peopleConn != null) {
					peopleConn.close();
				}
			} catch (SQLException e) {
				logger.warn("Falha no fechamento da conexao People para execucao das ddls", e);
			}
		}
		
		return ddlsResult;
	}
	
	/**
	 * Executa o scripts Vantive na base correspondente
	 * @param base	O tipo da base
	 * @return		O conjunto com o nome dos scripts Vantive que falharam
	 * @throws OmuException 
	 */
	public Map<String, String> runVantiveScripts(String base) {
		
		Connection vantiveConn = null;
		Map<String, String> scriptsResult = null;
		FilesDir vantiveScripts = omuPackage.getScriptsVantive();
		
		try {
		    if (!vantiveScripts.isEmpty()) {
    			logger.info("Inicio da execucao dos Scripts Vantive...");
    			vantiveConn = new OracleConnectionVantive(base).getConnection();
    			vantiveScripts.sortFileList();
    			scriptsResult = executeScriptsFiles(vantiveConn, vantiveScripts);
    			logger.info("Scripts Vantive executados");
		    } else {
                logger.info("Nao ha Scripts Vantive a serem executados");
            }
		} catch (SQLException e) {
		    logger.error("Falha na abertura da conexao Vantive para execucao dos scripts");
            logger.debug("", e);
		} catch (IOException e) {
		    logger.debug("", e);
        } catch (InterruptedException e) {
            logger.debug("", e);
        } finally {
			try {
				if (vantiveConn != null) {
					vantiveConn.close();
				}
			} catch (SQLException e) {
				logger.warn("Falha no fechamento da conexao Vantive para execucao dos scripts", e);
			}
		}
		
		return scriptsResult;
	}
	
	/**
	 * Executa as ddls Vantive na base correspondente
	 * @param base	O tipo da base
	 * @return		O conjunto com o nome das ddls Vantive que falharam
	 * @throws OmuException 
	 */
	public Map<String, String> runVantiveDdls(String base) {
		
		Connection vantiveConn = null;
		Map<String, String> ddlsResult = null;
		FilesDir ddlsVantive = omuPackage.getDdlsVantive();
		
		try {
		    if (!ddlsVantive.isEmpty()) {
    			logger.info("Inicio da execucao das Ddls Vantive...");
    			vantiveConn = new OracleConnectionVantive(base).getConnection();
    			ddlsResult = executeDdlFiles(vantiveConn, ddlsVantive);
    			logger.info("Ddls Vantive executadas");
		    } else {
                logger.info("Nao ha ddls Vantive a serem executadas");
            }
		} catch (SQLException e) {
		    logger.error("Falha na abertura da conexao Vantive para execucao das ddls");
            logger.debug("", e);
		} catch (IOException e) {
		    logger.debug("", e);
        } catch (InterruptedException e) {
            logger.debug("", e);
        } finally {
			try {
				if (vantiveConn != null) {
					vantiveConn.close();
				}
			} catch (SQLException e) {
				logger.warn("Falha no fechamento da conexao Vantive para execucao das ddls", e);
			}
		}
		
		return ddlsResult;
	}

	/**
	 * Deleta os arquivos contidos no diretorio
	 * @param demandDir o caminho do diretório
	 */
    public void deleteFiles(String demandDir) {
        for (File file : new File(demandDir).listFiles()) {
            OmuUtil.deleteFile(file);
        }
    }

	// private methods ----------------------------------------------------------------------------
	
	/**
	 * Adiciona os pacotes das demandas OMU ao processo
	 * @param packagesDirPath	O caminho do diretorio que contem os pacotes
	 * @return					A lista de demandas
	 * @throws IOException
	 */
	private List<OmuPackage> addDemandPackages(String packagesDirPath) throws IOException {
	    
		File packagesDir = new File(packagesDirPath);
		List<File> packagesFileList = new ArrayList<File>();
		OmuDemandPackages omuDemandPackages = new OmuDemandPackages();
		
		for (String packageName : packagesDir.list()) {
			
			File file = new File(packagesDir.getAbsolutePath(), packageName);
			if (file.isDirectory()) {
				packagesFileList.add(file);
			}
		}
		omuDemandPackages.readPackages(packagesFileList);
		
		return omuDemandPackages.getPackagesList();
	}

	/**
	 * Obtem os nomes dos Projetos People
	 * @return	Uma lista contendo o nome dos Projetos People
	 */
	private List<String> getPeopleProjectNames() {
		
		List<String> peopleProjectNames = new ArrayList<String>();
		List<String> peopleObjects = omuPackage.getObjectsFile().getPeopleObjects();
		for (String peopleObject : peopleObjects) {
			String[] validObjectSplit = peopleObject.split("::");
			if (validObjectSplit[0].equals("PEOPLE") && validObjectSplit[1].equals("Projeto")) {
				peopleProjectNames.add(validObjectSplit[2]);
			}
		}
		
		return peopleProjectNames;
	}

	/**
	 * Executa as ddls usando a conexao correspondente
	 * @param conn		A conexao com a base correspondente
	 * @param ddlsDir	O diretorio contendo as ddls
	 * @return			O conjunto com o nome das ddls que falharam
	 */
	private Map<String, String> executeDdlFiles(Connection conn, FilesDir ddlsDir) {

	    Map<String, String> ddlsResult = new HashMap<String, String>();

	    for (File ddlFile : ddlsDir.getFileList()) {
			
			boolean result = false;
			try {
				result = executeDdlFile(conn, ddlFile);
			} catch (IOException e) {
				ddlsResult.put(ddlFile.getName(), e.getMessage());
				logger.error("Falha na leitura da ddl: " + ddlFile.getName(), e);
			} catch (SQLException e) {
				ddlsResult.put(ddlFile.getName(), e.getMessage());
				logger.error("Falha na execucao da ddl: " + ddlFile.getName(), e);
			}
			
			if (result) {
			    ddlsResult.put(ddlFile.getName(), "OK");
			} else {
			    ddlsResult.put(ddlFile.getName(), "NO");
			}
		}
		
		return ddlsResult;
	}

	/**
	 * Executa o arquivo Ddl
	 * @param conn	A conexao com a base correspondente
	 * @param ddl	O arquivo Ddl
	 * @return		true se a execucao ocorreu com sucesso, false caso contrario
	 * @throws IOException 
	 * @throws SQLException 
	 */
	private boolean executeDdlFile(Connection conn, File ddl) throws IOException, SQLException {
		
		boolean isPackage = OmuUtil.isDdlPackage(ddl);
		boolean result = false;
		
		logger.info("Executando ddl: " + ddl.getName());
		if (isPackage) {
			result = executeDdlPackage(conn, ddl);
		} else {
			result = executeDdl(conn, ddl);
		}

		return result;
	}

	/**
	 * Executa a Ddl do tipo Package
	 * @param conn	A conexao com a base correspondente
	 * @param ddl	O arquivo Ddl
	 * @return		true se a execucao ocorreu com sucesso, false caso contrario
	 * @throws IOException
	 * @throws SQLException
	 */
	private boolean executeDdlPackage(Connection conn, File ddl) throws IOException, SQLException {
		
		String[] sql = OmuUtil.getPackageFileContents(ddl);
		boolean resultSpec = executeDdlContents(conn, sql[0]);
		boolean resultBody = executeDdlContents(conn, sql[1]);
		
		if (resultSpec && resultBody) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Executa a Ddl
	 * @param conn	A conexao com a base correspondente
	 * @param ddl	O arquivo Ddl
	 * @return		true se a execucao ocorreu com sucesso, false caso contrario
	 * @throws IOException
	 * @throws SQLException
	 */
	private boolean executeDdl(Connection conn, File ddl) throws IOException, SQLException {
		
		String sql = OmuUtil.getFileContents(ddl);
		
		return executeDdlContents(conn, sql);
	}
	
	/**
	 * Executa o conteudo da Ddl
	 * @param conn	A conexao com a base correspondente
	 * @param ddl	O conteudo da Ddl
	 * @return		true se a execucao ocorreu com sucesso, false caso contrario
	 * @throws SQLException
	 */
	private boolean executeDdlContents(Connection conn, String ddl) throws SQLException {
		
		PreparedStatement stm = null;
		boolean result = false;
		int numResult = -1;

		try {
    		stm = conn.prepareStatement(ddl);
    		numResult = stm.executeUpdate();
    		
    		if (numResult == 0) {
    			result = true;
    		}
		} finally {
    		try {
    			if (stm != null) {
    				stm.close();
    			}
    		} catch (SQLException e) {
    			logger.warn("Falha no fechamento do statement da execucao das ddls", e);
    		}
		}
		
		return result;
	}

	/**
	 * Executa os scripts usando a conexao correspondente
	 * @param conn			A conexao com a base correspondente
	 * @param scriptsDir	O diretorio contendo os scripts
	 * @return				O conjunto com o nome dos scripts que falharam
	 */
	private Map<String, String> executeScriptsFiles(Connection conn, FilesDir scriptsDir) {
		
	    Map<String, String> sqlFilesResult = new HashMap<String, String>();
		
	    for (File sqlFile : scriptsDir.getFileList()) {
			
			boolean result = false;
			try {
				result = executeScriptsFile(conn, sqlFile);
			} catch (IOException e) {
			    sqlFilesResult.put(sqlFile.getName(), e.getMessage());
				logger.error("Falha na leitura do script: " + sqlFile.getName(), e);
			} catch (SQLException e) {
			    sqlFilesResult.put(sqlFile.getName(), e.getMessage());
				logger.error("Falha na execucao do script: " + sqlFile.getName(), e);
			}
			
			if (result) {
			    sqlFilesResult.put(sqlFile.getName(), "OK");
            } else {
                sqlFilesResult.put(sqlFile.getName(), "NO");
            }
		}
		
		return sqlFilesResult;
	}
	
	/**
	 * Executa o conteudo do script usando a conexao correspondente
	 * @param conn		A conexao com a base correspondente
	 * @param script	O script contendo os comandos a serem executado
	 * @return			true se o conteudo do script foi executado com sucesso, false caso contrario
	 * @throws IOException 
	 * @throws SQLException 
	 */
	private boolean executeScriptsFile(Connection conn, File script) throws IOException, SQLException {
		
		String sql = OmuUtil.getFileContents(script);
		boolean result = false;
		
		logger.info("Executando script: " + script.getName());
		if (((sql.trim().toUpperCase().startsWith("BEGIN") || sql.trim().toUpperCase().startsWith("DECLARE")) && sql.trim().toUpperCase().endsWith("END;"))) {
			result = executeScripts(conn, sql);
		} else {
			result = executeScriptsInBatch(conn, sql.trim().split(";"));
		}
		
		return result;
	}

	/**
	 * Executa um comando sql
	 * @param conn	A conexao com o banco de dados
	 * @param sql	O comando sql a ser executado
	 * @return		true se a execucao ocorreu com sucesso, false caso contrario
	 * @throws SQLException
	 */
	private boolean executeScripts(Connection conn, String sql) throws SQLException {
		
		PreparedStatement stm = null;
		boolean result = false;
		
		try {
    		stm = conn.prepareStatement(sql);
    		if (!stm.execute()) {
    			result = true;
    		}
		} finally {
    		try {
    			if (stm != null) {
    				stm.close();
    			}
    		} catch (SQLException e) {
    			logger.warn("Falha no fechamento do statement da execucao dos scripts", e);
    		}
		}
		
		return result;
	}

	/**
	 * Executa varios comandos sql
	 * @param conn	A conexao com o banco de dados
	 * @param sqls	A lista de comandos sql a serem executados
	 * @return		true se a execucao ocorreu com sucesso, false caso contrario
	 * @throws SQLException
	 */
	private boolean executeScriptsInBatch(Connection conn, String[] sqls) throws SQLException {

		Statement stm = null;
		boolean result = true;
		
		try {
    		stm = conn.createStatement();
    		for (String sql : sqls) {
    			stm.addBatch(sql);
    		}
    		
    		int[] qtdResults = stm.executeBatch();
    		for (int qtdResult : qtdResults) {
    			if (qtdResult < 0) {
    				result = false;
    				break;
    			}
    		}
		} finally {
    		try {
    			if (stm != null) {
    				stm.close();
    			}
    		} catch (SQLException e) {
    			logger.warn("Falha no fechamento do statement da execucao em batch dos scripts", e);
    		}
		}
		
		return result;
	}

}
