package br.com.algartelecom.automacaoomudba.omu;

/**
 * 
 * @author mourao
 *
 */
public class OmuException extends Exception {

    private static final long serialVersionUID = 1L;

    public OmuException(Throwable t) {
        super(t);
    }

    public OmuException(String message) {
        super(message);
    }

    public OmuException(String message, Throwable t) {
        super(message, t);
    }

}