package br.com.algartelecom.automacaoomudba.omu.packages;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.algartelecom.automacaoomudba.utils.OmuUtil;

/**
 * Representa o pacote das demandas de OMU
 * @author mourao
 *
 */
public class OmuPackage {

	private FileOmuContents omuContents;
	private FileObjectsContents objectsContents;
	private FilesDir ddlsPeople;
	private FilesDir ddlsVantive;
	private FilesDir scriptsPeople;
	private FilesDir scriptsVantive;
	private FilesDir backupFiles;
	
	public OmuPackage() {
		super();
		omuContents = new FileOmuContents();
		objectsContents = new FileObjectsContents();
		ddlsPeople = new FilesDir();
		ddlsVantive = new FilesDir();
		scriptsPeople = new FilesDir();
		scriptsVantive = new FilesDir();
		backupFiles = new FilesDir();
	}
	
	public OmuPackage(File omuFile, File objectsFile, File ddlPeopleDir, File ddlVantiveDir, File scriptsPeopleDir, File scriptsVantiveDir, File backupFiles) throws IOException {
		super();
		this.omuContents = new FileOmuContents(omuFile);
		this.objectsContents = new FileObjectsContents(objectsFile);
		this.ddlsPeople = new FilesDir(ddlPeopleDir);
		this.ddlsVantive = new FilesDir(ddlVantiveDir);
		this.scriptsPeople = new FilesDir(scriptsPeopleDir);
		this.scriptsVantive = new FilesDir(scriptsVantiveDir);
		this.backupFiles = new FilesDir(backupFiles);
	}

	public FileOmuContents getOmuFile() {
		return omuContents;
	}

	public FileObjectsContents getObjectsFile() {
		return objectsContents;
	}

	public FilesDir getDdlsPeople() {
		return ddlsPeople;
	}

	public FilesDir getDdlsVantive() {
		return ddlsVantive;
	}

	public FilesDir getScriptsPeople() {
		return scriptsPeople;
	}

	public FilesDir getScriptsVantive() {
		return scriptsVantive;
	}
	
	public FilesDir getBackupFiles() {
		return backupFiles;
	}

	public void setOmuFile(FileOmuContents omuFileContents) {
		this.omuContents = omuFileContents;
	}

	public void setObjectsFile(FileObjectsContents objectsFileContents) {
		this.objectsContents = objectsFileContents;
	}

	public void setDdlsPeople(FilesDir ddlsPeople) {
		this.ddlsPeople = ddlsPeople;
	}

	public void setDdlsVantive(FilesDir ddlsVantive) {
		this.ddlsVantive = ddlsVantive;
	}

	public void setScriptsPeople(FilesDir scriptsPeople) {
		this.scriptsPeople = scriptsPeople;
	}

	public void setScriptsVantive(FilesDir scriptsVantive) {
		this.scriptsVantive = scriptsVantive;
	}
	
	public void setBackupFiles(FilesDir backupFiles) {
		this.backupFiles = backupFiles;
	}

	/**
	 * Adiciona os arquivos de um pacote ao pacote atual 
	 * @param packageToMerge	O pacote a ser adicionado ao pacote atual
	 */
	public void mergePackage(OmuPackage packageToMerge) {
		omuContents.mergeFileOMUFileContents(packageToMerge.getOmuFile());
		objectsContents.mergeFileObjectsContents(packageToMerge.getObjectsFile());
		ddlsPeople.mergeFilesDir(packageToMerge.getDdlsPeople());
		ddlsVantive.mergeFilesDir(packageToMerge.getDdlsVantive());
		scriptsPeople.mergeFilesDir(packageToMerge.getScriptsPeople());
		scriptsVantive.mergeFilesDir(packageToMerge.getScriptsVantive());
		backupFiles.mergeFilesDir(packageToMerge.getBackupFiles());
	}
	
	/**
	 * Escreve o pacote no caminho especificado
	 * @param path		O caminho onde o pacote ser escrito
	 * @param basename	O nome da base
	 * @param demand    O nome da demanda
	 * @return			O arquivo do pacote
	 * @throws IOException
	 */
	public File writePackage(String path, String basename, String demand) throws IOException {
		
		String demandName = new SimpleDateFormat("ddMMyyyy").format(new Date()) + "_OMU_" + basename.toUpperCase() + "_" + demand.toUpperCase();
		
		File packageDir = new File(path, demandName);
		packageDir.mkdirs();
		File backupFilesDir = new File(packageDir, "BackupFiles");
		backupFilesDir.mkdirs();
		File scriptsVantiveDir = new File(packageDir, "Scripts_Vantive");
		scriptsVantiveDir.mkdirs();
		File scriptsPeopleDir = new File(packageDir, "Scripts_People");
		scriptsPeopleDir.mkdirs();
		File ddlsVantiveDir = new File(packageDir, "DDLs_Vantive");
		ddlsVantiveDir.mkdirs();
		File ddlsPeopleDir = new File(packageDir, "DDLs_People");
		ddlsPeopleDir.mkdirs();
		File objectsFile = new File(packageDir, "objects.txt");
		File omuFile = null;
		
		writeFileList(backupFilesDir.getAbsolutePath(), backupFiles.getFileList());
		writeFileList(scriptsVantiveDir.getAbsolutePath(), scriptsVantive.getFileList());
		writeFileList(scriptsPeopleDir.getAbsolutePath(), scriptsPeople.getFileList());
		writeFileList(ddlsVantiveDir.getAbsolutePath(), ddlsVantive.getFileList());
		writeFileList(ddlsPeopleDir.getAbsolutePath(), ddlsPeople.getFileList());
		OmuUtil.writeFileContents(objectsFile, objectsContents.getFileObjectsContents());
		if (!omuContents.getFileOMUContents().isEmpty()) {
			omuFile = new File(packageDir, demandName + ".omu");
			OmuUtil.writeOmuFileContents(omuFile, omuContents.getFileOMUContents());
		}
		
		return packageDir;
	}
	
	/**
	 * Escreve uma lista de arquivos no local especificado
	 * @param path		O caminho onde os arquivo serao escritos
	 * @param filelist	A lista de arquivos
	 * @throws IOException
	 */
	private void writeFileList(String path, List<File> filelist) throws IOException {
		for (File file : filelist) {
			OmuUtil.writeFile(file, path);
		}
	}

}
