DECLARE

	CURSOR C1 IS
		SELECT object_name,'alter ' || rtrim(object_type) || ' ' || owner || '.' || rtrim(object_name) || ' compile' VCOMANDO
		FROM dba_objects
		WHERE status = 'INVALID'
			AND object_type NOT LIKE 'PACKAGE%' 
			AND object_type <> 'JAVA CLASS';

	CURSOR C2 IS
		SELECT object_name,'alter package ' || owner || '.' || rtrim(object_name) || ' compile package' VCOMANDO
		FROM dba_objects
		WHERE status = 'INVALID'
			AND object_type = 'PACKAGE';

	CURSOR C3 IS
		SELECT object_name,'alter package ' || owner || '.' || rtrim(object_name) || ' compile body' VCOMANDO
		FROM dba_objects
		WHERE status = 'INVALID'
			AND object_type = 'PACKAGE BODY';

	VCOMANDO VARCHAR2(4000);
  	STR_OUT VARCHAR2(32767);
  	LOOP_COUNT NUMBER DEFAULT 0;

BEGIN 

	SELECT 'alter session set events=''10933 trace name context forever, level 512''' INTO vcomando FROM dual;

	EXECUTE IMMEDIATE vcomando;

	SELECT 'alter system flush shared_pool' INTO vcomando FROM dual;

	FOR R1 IN C1 LOOP
		BEGIN
			EXECUTE IMMEDIATE r1.VCOMANDO;
			EXCEPTION
	 			WHEN OTHERS THEN  -- handles all other errors
            		STR_OUT:=r1.object_name||' -> ERRO: '||SQLERRM;
            		LOOP_COUNT:=0;
            		WHILE LOOP_COUNT < LENGTH(STR_OUT)
            		LOOP
              			DBMS_OUTPUT.PUT_LINE(SUBSTR(STR_OUT, LOOP_COUNT+1, 255));
              			LOOP_COUNT:=LOOP_COUNT+255;
            		END LOOP;
		  			NULL;
		END;
	END LOOP;


	FOR R2 IN C2 LOOP
		BEGIN
			EXECUTE IMMEDIATE r2.VCOMANDO;
			EXCEPTION
				WHEN OTHERS THEN  -- handles all other errors
				  	STR_OUT:=r2.object_name||' -> ERRO: '||SQLERRM;
				  	LOOP_COUNT:=0;
				  	WHILE LOOP_COUNT < LENGTH(STR_OUT)
				  	LOOP
				    	DBMS_OUTPUT.PUT_LINE(SUBSTR(STR_OUT, LOOP_COUNT+1, 255));
				    	LOOP_COUNT:=LOOP_COUNT+255;
				 	END LOOP;
					NULL;
		END;
	END LOOP;

	FOR R3 IN C3 LOOP
		BEGIN
			EXECUTE IMMEDIATE r3.VCOMANDO;
			EXCEPTION
				WHEN OTHERS THEN  -- handles all other errors
          			STR_OUT:=r3.object_name||' -> ERRO: '||SQLERRM;
          			LOOP_COUNT:=0;
          			WHILE LOOP_COUNT < LENGTH(STR_OUT)
          			LOOP
            			DBMS_OUTPUT.PUT_LINE(SUBSTR(STR_OUT, LOOP_COUNT+1, 255));
            			LOOP_COUNT:=LOOP_COUNT+255;
          			END LOOP;
					NULL;
		END;
	END LOOP;

END;
