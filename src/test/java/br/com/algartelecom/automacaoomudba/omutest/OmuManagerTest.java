package br.com.algartelecom.automacaoomudba.omutest;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import br.com.algartelecom.automacaoomudba.omu.OmuException;
import br.com.algartelecom.automacaoomudba.omu.OmuManager;
import br.com.algartelecom.automacaoomudba.utils.OmuProperties;

public class OmuManagerTest {

	@Ignore
	@Test
	public void testExtractDemands() {
		OmuManager manager = new OmuManager();
		manager.extractDemands(getClass().getClassLoader().getResource("zipFilesToExtract").getFile(), getClass().getClassLoader().getResource("extractedFiles").getFile());
		assertTrue(new File(getClass().getClassLoader().getResource("extractedFiles").getFile()).list().length != 0);
	}
	
	@Ignore
	@Test
	public void testCreateOMUPackageToProcess() throws OmuException {
		OmuManager manager = new OmuManager();
		File omuPackage = manager.createOmuPackageToProcess(getClass().getClassLoader().getResource("extractedFiles").getFile(), 
				getClass().getClassLoader().getResource("").getFile(), "teste", "teste");
		assertNotNull(omuPackage);
		assertNotNull(manager.getOmuPackage());
		assertFalse(manager.getOmuPackage().getDdlsPeople().getFileList().isEmpty());
		assertFalse(manager.getOmuPackage().getOmuFile().getFileOMUContentsList().isEmpty());
		assertFalse(manager.getOmuPackage().getObjectsFile().getFileObjectsContents().isEmpty());
	}

	@Ignore
	@Test
	public void testSendOmuFile() throws OmuException {
		File file = new File(getClass().getClassLoader().getResource("OMU_TESTE.omu").getFile());
		OmuManager manager = new OmuManager();
//		manager.sendOmuFile(file, OmuProperties.getProperty("omu.in.omufilepath"), "des");
	}

	@Ignore
	@Test
	public void testChangeOmuParameters() throws OmuException {
		OmuManager manager = new OmuManager();
//		manager.changeOmuParameters("export", "homologacao", "testeOMU.omu", "des");
	}

	@Ignore
	@Test
	public void testExecuteOmu() throws IOException, OmuException {
		OmuManager manager = new OmuManager();
		File omuFile = new File(getClass().getClassLoader().getResource("OMU_TESTE.omu").getFile());
//		manager.sendOmuFile(omuFile , OmuProperties.getProperty("omu.in.omufilepath"), "des");
//		manager.changeOmuParameters("export", "simulado", omuFile.getName(), "des");
//		int result = manager.executeOmu("des");
//		assertEquals(0, result);
	}

	/*@Ignore
	@Test
	public void testGetInvalidPeopleObjectsNumber() throws OmuException {
		OmuManager manager = new OmuManager();
		assertNotEquals(0, manager.getInvalidPeopleObjectsNumber("des"));
	}
	
	@Ignore
	@Test
	public void testGetInvalidVantiveObjectsNumber() throws OmuException {
		OmuManager manager = new OmuManager();
		assertNotEquals(0, manager.getInvalidVantiveObjectsNumber("des"));
	}

	@Ignore
	@Test
	public void testCompileInvalidPeopleObjects() throws OmuException {
		OmuManager manager = new OmuManager();
		String result = manager.compileInvalidPeopleObjects("des");
		assertNotNull(result);
	}
	
	@Ignore
	@Test
	public void testCompileInvalidVantiveObjects() throws OmuException {
		OmuManager manager = new OmuManager();
		String result = manager.compileInvalidVantiveObjects("des");
		assertNotNull(result);
	}*/
	
	@Ignore
	@Test
	public void testRunPeopleScripts() throws OmuException {
		OmuManager manager = new OmuManager();
		manager.createOmuPackageToProcess(getClass().getClassLoader().getResource("testeFiles").getFile(), getClass().getClassLoader().getResource("").getPath(), "teste", "teste");
		Map<String, String> scriptsFail = manager.runPeopleScripts("des");
		assertTrue(scriptsFail.isEmpty());
	}

	@Ignore
	@Test
	public void testRunPeopleDDLs() throws OmuException {
		OmuManager manager = new OmuManager();
		manager.createOmuPackageToProcess(getClass().getClassLoader().getResource("testeFiles").getFile(), getClass().getClassLoader().getResource("").getPath(), "teste", "teste");
		Map<String, String> ddlsFail = manager.runPeopleDdls("des");
		assertTrue(ddlsFail.isEmpty());
	}

	@Ignore
	@Test
	public void testRunVantiveScripts() throws OmuException {
		OmuManager manager = new OmuManager();
		manager.createOmuPackageToProcess(getClass().getClassLoader().getResource("testeFiles").getFile(), getClass().getClassLoader().getResource("").getPath(), "teste", "teste");
		Map<String, String> scriptsFail = manager.runVantiveScripts("des");
		assertTrue(scriptsFail.isEmpty());
		
	}

	@Ignore
	@Test
	public void testRunVantiveDDLs() throws OmuException {
		OmuManager manager = new OmuManager();
		manager.createOmuPackageToProcess(getClass().getClassLoader().getResource("testeFiles").getFile(), getClass().getClassLoader().getResource("").getPath(), "teste", "teste");
		Map<String, String> ddlsFail = manager.runVantiveDdls("des");
		assertTrue(ddlsFail.isEmpty());
	}

}
