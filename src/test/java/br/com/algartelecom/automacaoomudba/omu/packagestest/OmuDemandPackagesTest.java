package br.com.algartelecom.automacaoomudba.omu.packagestest;

import static org.junit.Assert.assertFalse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.algartelecom.automacaoomudba.omu.packages.OmuDemandPackages;

public class OmuDemandPackagesTest {

	@Ignore
	@Test
	public void testReadPackages() throws IOException {
		List<File> packageFiles = new ArrayList<File>();
		File demandsDir = new File(getClass().getClassLoader().getResource("extractedFiles").getFile());
		for (String filename : demandsDir.list()) {
			packageFiles.add(new File(demandsDir, filename));
		}
		
		OmuDemandPackages demandPackages = new OmuDemandPackages();
		demandPackages.readPackages(packageFiles);
		assertFalse(demandPackages.getPackagesList().isEmpty());
	}

}
