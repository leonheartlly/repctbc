package br.com.algartelecom.automacaoomudba.omu.packagestest;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Ignore;
import org.junit.Test;

import br.com.algartelecom.automacaoomudba.omu.packages.FilesDir;

public class FilesDirTest {

	@Ignore
	@Test
	public void testFillFileList() {
		FilesDir filesDir = new FilesDir();
		filesDir.fillFileList(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5205/DDLs_People").getFile()));
		assertFalse(filesDir.getFileList().isEmpty());
		assertFalse(filesDir.getFilenamesList().isEmpty());
	}

	@Ignore
	@Test
	public void testMergeFilesDir() {
		FilesDir filesDir1 = new FilesDir(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5205/DDLs_People").getFile()));
		FilesDir filesDir2 = new FilesDir(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5205/DDLs_People").getFile()));
		//int qtdFiles1 = filesDir1.getFileList().size();
		//int qtdFiles2 = filesDir2.getFileList().size();
		//int qtdFilenames1 = filesDir1.getFilenamesList().size();
		//int qtdFilenames2 = filesDir2.getFilenamesList().size();
		filesDir1.mergeFilesDir(filesDir2);
		//assertTrue(filesDir1.getFileList().size() == (qtdFiles1+qtdFiles2));
		//assertTrue(filesDir1.getFilenamesList().size() == (qtdFilenames1+qtdFilenames2));
	}
	
	@Ignore
	@Test
	public void tesSortFileList() {
		FilesDir filesDir = new FilesDir(new File(getClass().getClassLoader().getResource("testeFiles/scripts_order").getFile()));
		for (File file : filesDir.getFileList()) {
			System.out.println(file.getName());
		}
		filesDir.sortFileList();
		System.out.println();
		for (File file : filesDir.getFileList()) {
			System.out.println(file.getName());
		}
	}
	
}
