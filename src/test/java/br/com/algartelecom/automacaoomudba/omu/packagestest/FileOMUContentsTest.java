package br.com.algartelecom.automacaoomudba.omu.packagestest;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;

import br.com.algartelecom.automacaoomudba.omu.packages.FileOmuContents;

public class FileOMUContentsTest {

	@Ignore
	@Test
	public void testSetFileOMUContents() throws IOException {
		FileOmuContents fileOMUContents = new FileOmuContents(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/omu_file.omu").getFile()));
		int qtdContents = fileOMUContents.getVapps().size();
		assertTrue(qtdContents > 0);
	}

	@Ignore
	@Test
	public void testMergeFileOMUFileContents() throws IOException {
		FileOmuContents fileOMUContents1 = new FileOmuContents(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/omu_file.omu").getFile()));
		FileOmuContents fileOMUContents2 = new FileOmuContents(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5202/omu_file.omu").getFile()));
		int qtdContents = fileOMUContents1.getVapps().size();
		fileOMUContents1.mergeFileOMUFileContents(fileOMUContents2);
		assertTrue(qtdContents < fileOMUContents1.getVapps().size());
	}
	
	@Ignore
	@Test
	public void testGetFileOMUContents() throws IOException {
		FileOmuContents fileOMUContents1 = new FileOmuContents(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/omu_file.omu").getFile()));
		FileOmuContents fileOMUContents2 = new FileOmuContents(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5202/omu_file.omu").getFile()));
		fileOMUContents1.mergeFileOMUFileContents(fileOMUContents2);
		assertNotNull(fileOMUContents1.getFileOMUContents());
	}
	
}
