package br.com.algartelecom.automacaoomudba.omu.packagestest;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;

import br.com.algartelecom.automacaoomudba.omu.packages.OmuPackage;

public class OmuPackageTest {

	@Ignore
	@Test
	public void testWritePackage() throws IOException {
		
		File omuFile = new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/omu_file.omu").getFile());
		File objectsFile = new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/objects").getFile());
		File ddlPeopleDir = null;
		File ddlVantiveDir = new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/DDLs_Vantive").getFile());
		File scriptsPeopleDir = null;
		File scriptsVantiveDir = null;
		File backupFiles = new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/BackupFiles").getFile());
		
		OmuPackage omuPackage = new OmuPackage(omuFile, objectsFile, ddlPeopleDir, ddlVantiveDir, scriptsPeopleDir, scriptsVantiveDir, backupFiles);
		File file = omuPackage.writePackage(getClass().getClassLoader().getResource("").getFile(), "teste", "teste");
		assertNotNull(file);
	}
	
	@Ignore
	@Test
	public void testWritePackageWithoutOmuFile() throws IOException {
		
		File omuFile = null;
		File objectsFile = new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/objects").getFile());
		File ddlPeopleDir = null;
		File ddlVantiveDir = new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/DDLs_Vantive").getFile());
		File scriptsPeopleDir = null;
		File scriptsVantiveDir = null;
		File backupFiles = new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/BackupFiles").getFile());
		
		OmuPackage omuPackage = new OmuPackage(omuFile, objectsFile, ddlPeopleDir, ddlVantiveDir, scriptsPeopleDir, scriptsVantiveDir, backupFiles);
		omuPackage.writePackage(getClass().getClassLoader().getResource("").getFile(), "teste", "teste");
		File file = omuPackage.writePackage(getClass().getClassLoader().getResource("").getFile(), "teste", "teste");
		assertNull(file);
	}

}
