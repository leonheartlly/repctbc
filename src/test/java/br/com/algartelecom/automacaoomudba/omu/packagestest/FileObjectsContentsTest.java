package br.com.algartelecom.automacaoomudba.omu.packagestest;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;

import br.com.algartelecom.automacaoomudba.omu.packages.FileObjectsContents;

public class FileObjectsContentsTest {

	@Ignore
	@Test
	public void testSetFileObjectsContents() throws IOException {
		FileObjectsContents fileObjectsContents = new FileObjectsContents(new File(getClass().getClassLoader().getResource("objects_test").getFile()));
		assertFalse(fileObjectsContents.getVantiveObjects().isEmpty());
		assertFalse(fileObjectsContents.getPeopleObjects().isEmpty());
	}

	@Ignore
	@Test
	public void testMergeFileObjectsContents() throws IOException {
		FileObjectsContents fileObjectsContents1 = new FileObjectsContents(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/objects").getFile()));
		FileObjectsContents fileObjectsContents2 = new FileObjectsContents(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5205/objects").getFile()));
		assertTrue(fileObjectsContents1.getPeopleObjects().isEmpty());
		fileObjectsContents1.mergeFileObjectsContents(fileObjectsContents2);
		assertFalse(fileObjectsContents1.getPeopleObjects().isEmpty());
	}
	
}
