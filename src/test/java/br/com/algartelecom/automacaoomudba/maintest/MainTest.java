package br.com.algartelecom.automacaoomudba.maintest;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.algartelecom.automacaoomudba.omu.OmuException;
import br.com.algartelecom.automacaoomudba.omu.OmuManager;
import br.com.algartelecom.automacaoomudba.utils.OmuProperties;

public class MainTest {

	private Logger logger = LoggerFactory.getLogger(MainTest.class);
	
	@Ignore
	@Test
	public void testOmu() {

		/*OmuManager manager = new OmuManager();
		
		try {
			
			manager.extractDemands("/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/omu/demands", 
					"/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/omu/packages");
			
			File omuFile = manager.createOmuPackageToProcess("/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/omu/packages", 
					"/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/omu/backup", "teste_automacao");
			
			if (omuFile != null) {
				
				manager.sendOmuFile(omuFile, OmuProperties.getProperty("omu.in.omufilepath"), "des");
				manager.changeOmuParameters("export", "simulado", omuFile.getName(), "des");
				
				if (manager.executeOmu("des") == 0) {
					
					File exportLogFile = manager.getOmuLogFile("/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/omu/backup", "des");
					if (exportLogFile != null) {
						
						if (manager.processSuccess(exportLogFile)) {
						
							manager.changeOmuParameters("import", "simulado", omuFile.getName(), "sim");
							if (manager.executeOmu("sim") == 0) {
								
								File importLogFile = manager.getOmuLogFile("/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/omu/backup", "sim");
								if (importLogFile != null) {
									
									if (manager.processSuccess(importLogFile)) {
										logger.debug("Processo omu finalizado com sucesso");
									} else {
										logger.error("Falha no IMPORT omu");
									}
								} else {
									logger.error("Nao foi possivel obter o log do IMPORT de dados");
								}							
							} else {
								logger.error("Falha na execucao do IMPORT omu");
							}
						} else {
							logger.error("Falha no EXPORT omu");
						}
					} else {
						logger.error("Nao foi possivel obter o log do EXPORT de dados");
					}
					
				} else {
					logger.error("Falha na execucao do EXPORT omu");
				}
				
			} else {
				logger.error("Falha na criacao do arquivo omu");
			}
			
		} catch (OmuException e) {
			logger.error("", e);
		} finally {
			
		}*/
		
	}
	
	@Ignore
	@Test
	public void testScripsDdls() {
		
		OmuManager manager = new OmuManager();
		
		try {
			
			manager.extractDemands("/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/scripts_ddls/demands", 
					"/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/scripts_ddls/packages");
			
			File omuFile = manager.createOmuPackageToProcess("/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/scripts_ddls/packages", 
					"/home/mourao/sources/works/eclipse/algar-telecom-automacaoomudba/src/test/resources/mainTest/scripts_ddls/backup", "teste", "teste");

			if (omuFile != null) {
				logger.debug("Arquivo omu: " + omuFile.getName());
			}
			
			Map<String, String> scriptsFail = manager.runVantiveScripts("des");
			/*for (String script : scriptsFail) {
				logger.debug(script);
			}*/
			
			Map<String, String> ddlsResult = manager.runVantiveDdls("des");
			/*for (String ddl : ddlsFail) {
				logger.debug(ddl);
			}*/
			
		} catch (OmuException e) {
			logger.error("", e);
		} finally {
			
		}
	}

}
