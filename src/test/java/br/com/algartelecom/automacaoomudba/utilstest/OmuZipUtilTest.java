package br.com.algartelecom.automacaoomudba.utilstest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Ignore;
import org.junit.Test;

import br.com.algartelecom.automacaoomudba.utils.OmuZipUtil;

public class OmuZipUtilTest {

	@Ignore
	@Test
	public void testExtractedFileOk() {
		File fileToExtract = new File(getClass().getClassLoader().getResource("zipFilesToExtract/EVL-5204.zip").getFile()); 
		String pathToExtract = getClass().getClassLoader().getResource("extractedFiles").getPath(); 
		assertTrue(OmuZipUtil.extractFile(fileToExtract, pathToExtract));
	}

	@Ignore
	@Test
	public void testExtractedFileWithNotZipFile() {
		File fileToExtract = new File(getClass().getClassLoader().getResource("notZipFilesToExtracted/EVL-5204.tar.gz").getFile());
		String pathToExtract = getClass().getClassLoader().getResource("extractedFiles").getPath();
		assertFalse(OmuZipUtil.extractFile(fileToExtract, pathToExtract));
	}
	
}
