package br.com.algartelecom.automacaoomudba.utilstest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import br.com.algartelecom.automacaoomudba.omu.packages.OmuPackage;
import br.com.algartelecom.automacaoomudba.utils.OmuUtil;

public class OmuUtilTest {

    @Ignore
    @Test
    @SuppressWarnings("unchecked")
    public void testGetUpdateObjects() {
        
        Map<String, String> scripts = new HashMap<String, String>();
        scripts.put("script01", "OK");
        scripts.put("script02", "NO");
        scripts.put("script03", "EXCEPTION");
        Map<String, String> ddls = new HashMap<String, String>();
        ddls.put("ddl01", "OK");
        ddls.put("ddl02", "NO");
        ddls.put("ddl03", "EXCEPTION");
        
        String updateObjects = OmuUtil.getUpdateObjects(scripts, ddls);
        assertNotNull(updateObjects);
        assertEquals("script01\nddl01\n", updateObjects);
        
    }
    
	@Ignore
	@Test
	public void testGetOriginBase() {
		assertEquals("des", OmuUtil.getOriginBase("simulado"));
		assertEquals("sim", OmuUtil.getOriginBase("homologacao"));
	}
	
	@Ignore
	@Test
	public void testGetDestinationBase() {
		assertEquals("sim", OmuUtil.getDestinationBase("simulado"));
		assertEquals("hml", OmuUtil.getDestinationBase("homologacao"));
	}
	
	@Ignore
	@Test
	public void testCreateOmuDemandPackage() throws IOException {
		
		File packageFile = new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5205").getFile());
		OmuPackage omuDemandPackage = OmuUtil.createOmuDemandPackage(packageFile);
		assertTrue(!omuDemandPackage.getDdlsPeople().getFileList().isEmpty() && !omuDemandPackage.getObjectsFile().getPeopleObjects().isEmpty());
	}

	@Ignore
	@Test
	public void testGetOmuParameters() {
//		assertEquals("or8\naix\nE\n/u00/app/oracle8i/product/8.1.7\ncrmdes01\nswbapps\ndesenv01\ndesenv01\n/home/vantive/omu/data\n/home/vantive/omu/log\n/home/vantive/omu/teste.omu\nYes\n", OmuUtil.getOmuParameters("export", "simulado", "teste.omu"));
//		assertEquals("or8\naix\nE\n/u00/app/oracle8i/product/8.1.7\ncrmdes01\nswbapps\ndesenv01\ndesenv01\n/home/vantive/omu/data\n/home/vantive/omu/log\n/home/vantive/omu/teste.omu\nYes\n", OmuUtil.getOmuParameters("export", "homologacao", "teste.omu"));
//		assertEquals("or8\naix\nI\n/u00/app/oracle8i/product/8.1.7\ncrmdes01\nswbapps\ndesenv01\ndesenv01\n/home/vantive/omu/data\n/home/vantive/omu/log\nYes\n/home/vantive/omu/backup\n/home/vantive/omu/teste.omu\nYes\n", OmuUtil.getOmuParameters("import", "simulado", "teste.omu"));
//		assertEquals("or8\naix\nI\n/u00/app/oracle8i/product/8.1.7\ncrmdes01\nswbapps\ndesenv01\ndesenv01\n/home/vantive/omu/data\n/home/vantive/omu/log\nYes\n/home/vantive/omu/backup\n/home/vantive/omu/teste.omu\nYes\n", OmuUtil.getOmuParameters("import", "homologacao", "teste.omu"));
	}
	
	@Ignore
	@Test
	public void testGetPackageFileContents() throws IOException {
		
		String[] packageContent = OmuUtil.getPackageFileContents(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/DDLs_Vantive/PC_ATIVACAO_AUTO_PACOTE_DADOS-crmdbase.sql").getFile()));
		assertFalse(packageContent[0].endsWith("/"));
		assertFalse(packageContent[1].endsWith("/"));
	}
	
	@Ignore
	@Test
	public void testWriteFile1() throws IOException {
		
		File file = OmuUtil.writeFile(getClass().getClassLoader().getResource("testWrite").getPath(), "Testando!!!");
		assertNotNull(file);
		assertEquals(getClass().getClassLoader().getResource("testWrite").getPath(), file.getAbsolutePath());
	}
	
	@Ignore
	@Test
	public void testWriteFile2() throws IOException {
		OmuUtil.writeFile(new File(getClass().getClassLoader().getResource("extractedFiles/EVL-5203/BackupFiles").getPath()), getClass().getClassLoader().getResource("").getPath());
	}
	
	@Ignore
	@Test
	public void testDeleteFile() throws IOException {
		File file = new File(getClass().getClassLoader().getResource("").getPath(), "deleteme");
		if (!file.exists()) {
			assertTrue(file.createNewFile());
		}
		OmuUtil.deleteFile(file);
		assertFalse(file.exists());
	}
	
}
