package br.com.algartelecom.automacaoomudba.utilstest;

import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

import br.com.algartelecom.automacaoomudba.utils.OmuProperties;

public class OmuPropertiesTest {

	@Ignore
	@Test
	public void testGetProperty() {
		assertTrue("/home/vantive/omu/backup".equals(OmuProperties.getProperty("omu.in.backup.dir")));
		assertTrue("mail.transport.protocol".equals(OmuProperties.getProperty("mail.protocol.transfer")));
	}

}
